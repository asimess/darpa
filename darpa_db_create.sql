CREATE TABLE papers (
	paper_id bigserial PRIMARY KEY,
	doi varchar (100),
	pubmed_id integer,
	source_id integer,
	id_in_source integer,
	title varchar (800) UNIQUE,
	year smallint,
	outlet_id integer,
	volume varchar(100),
	number integer,
	pages varchar (100),
	reference_no integer,
	UNIQUE(doi,title)
);


CREATE TABLE outlet (
	outlet_id serial PRIMARY KEY,
	outlet_name varchar (400) UNIQUE,
	type varchar(4),
	issn varchar(20),
	eissn varchar(20),
	isbn varchar(20),
	impact_factor_1_year numeric(3,3),
	impact_factor_5_year numeric(3,3),
	impact_factor_update_date DATE
);


CREATE TABLE outlet_fields (
    outlet_id integer NOT NULL,
	field_id integer NOT NULL,
	UNIQUE(outlet_id,field_id)
);

CREATE TABLE fields_of_study (
	field_id serial PRIMARY KEY,
	field_description varchar(100)
);

CREATE TABLE abstract (
	paper_id bigint UNIQUE,
	abstract varchar(6000)
);

CREATE TABLE models (
	model_id serial PRIMARY KEY,
	model_name varchar (50),
	UNIQUE(model_name)
);

CREATE TABLE runs (
	run_id serial PRIMARY KEY,
	dataset_id int,
	primitive_id int,
	accuracy real,
	dataset_name varchar (90),
	primitive_name varchar (90),
	UNIQUE(dataset_id,primitive_id)
);

CREATE TABLE datasets (
	dataset_id serial PRIMARY KEY,
	dataset_name varchar (90),
	dataset_normal varchar (90),
	dataset_normal2 varchar (90),
	description varchar (1000),
	title varchar(220),
	top1_accuracy real,
	top1_primitive_name varchar (90),
	top1_primitive_normal_name varchar (90),
	top1_primitive_id int,
	top1_primitive_family varchar (20),
	top1_primitive_normal_name2 varchar (90),
	top2_accuracy real,
	top2_primitive_name varchar (90),
	top2_primitive_normal_name varchar (90),
	top2_primitive_id int,
	top2_primitive_family varchar (20),
	top2_primitive_normal_name2 varchar (90),
	top3_accuracy real,
	top3_primitive_name varchar (90),
	top3_primitive_normal_name varchar (90),
	top3_primitive_id int,
	top3_primitive_family varchar (20),
	top3_primitive_normal_name2 varchar (90),
	top4_accuracy real,
	top4_primitive_name varchar (90),
	top4_primitive_id int,
	top4_primitive_normal_name varchar (90),
	top4_primitive_family varchar (20),
	top4_primitive_normal_name2 varchar (90),
	top5_accuracy real,
	top5_primitive_name varchar (90),
	top5_primitive_id int,
	top5_primitive_family varchar (20),
	top5_primitive_normal_name varchar (90),
	top5_primitive_normal_name2 varchar (90),
	UNIQUE(dataset_name)
);

CREATE TABLE primitives (
	primitive_id serial PRIMARY KEY,
	primitive_name varchar (90),
	primitive_normal_name varchar (90),
	primitive_normal_name2 varchar (90),
	primitive_normal_name3 varchar (90),
	description varchar (200),
	model_family varchar(20),
	UNIQUE(primitive_name)
);


CREATE TABLE sources (
	source_id serial PRIMARY KEY,
	source_name varchar (30)
);

CREATE TABLE papers_keywords (
	paper_id bigint,
	keyword_id bigint,
	keyword_type varchar(2),
	dictionary_version int default 0,
	UNIQUE(paper_id,keyword_id)
);

CREATE TABLE keywords (
	keyword_id bigserial PRIMARY KEY,
	normalized_keyword_string varchar(90) UNIQUE,
	keyword_type varchar(2)
);

CREATE TABLE keywords_primitives_map (
    map_id serial PRIMARY KEY,
    primitive_name varchar(90),
    keyword_name varchar(90),
    model_family varchar(20),
    UNIQUE(keyword_name,primitive_name)
)

