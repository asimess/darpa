# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
project: Darpa BGU project    
Task 1: create papers db    
Task 2: Generate embedding representation of problems and models  
Task 3: Get primitives for a given dataset  

### Files description
darpa_createdb.py: python code to create postgres db. The input files are unrared. Currently implemented for Engineering Village. 
darpadbconfig.py: configuration file for the python code (e.g. source to db fields mapping, types mapping etc)
create_glove_corpus.py: creates a corpus file for GloVe word embedding from database  
evaluate_embedding.py: similarity tests for apps and models  
translate_embedding.py: translate embedding files to tensor board format for embedding visualization  
get_primitives.py: interface for getting dataset (and problem id) and returnning an ordered list of primitive ids  
darpa_db_create.sql: postgres db strcuture, used to create the db. Please update when you update the db structure.   
clean_db.sql: cleans the db  
AlgorithmList.txt, ApplicationsList.txt: list of algorithms (before synonyms normalization)  
DatabaseSchemaStage1.docx: database schema guidance

### How do I get set up? ###
db schema : darpa_db_create.sql  
download files : install go and use drive to download from Google drive. Use unrar to unrar files. 
insert papers to db: darpa_createdb.py, darpadbconfig.py (Engineering Village WIP)  

#### links to install go, download from Drive and unrar
install go: https://www.digitalocean.com/community/tutorials/how-to-install-go-1-6-on-ubuntu-16-04  
install drive: https://github.com/odeke-em/drive/blob/master/README.md  
install rar: https://askubuntu.com/questions/566407/whats-the-easiest-way-to-unrar-a-file-on-ubuntu-12-04/566409

#### Useful commands ###

*Unrar multiple files:*          
for file in *.rar; do unrar x ${file}; done;    

*Google drive init:*             
drive init ~/gdrive   (there explain how to provide the key)
cd ~/gdrive    

*Pull files from Google drive:*           
drive pull -force AcademicPapersData/EngineeringVillage   
