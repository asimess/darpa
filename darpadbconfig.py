#!/usr/bin/env python
## paper sources
engineering_vilage = {
    'doi':'DOI',
    'title':'Title',
    'year':'Publication year',
    'volume':'Volume',
    'number':'Issue',
    'pages':'Pages',
    'source_name':'Engineering Village',
    'type':'Document type', #requires pre-processing
    'issn':'ISSN',
    'eissn':'E-ISSN',
    'isbn':'ISBN',
    'fields':'Classification code', #requires pre-processing
    'keywords_authors':'Controlled/Subject terms', #requires pre-processing
    'keywords_source':'Uncontrolled terms',
    'outlet_name':'Source',
    'abstract':'Abstract',
    'delim_char':',',
    'format':'csv',
    'author_keyword_delimiter':'-',
    'source_keyword_delimiter':'-',
    'reference_no':'Number of references'
}

## primitive sources
fernandes_primitives = {
    'primitive_name':'problema',
    'file_name':'Fernandes_datasete.csv',
    'clean_string':'_',
    'delim_char':','
}

## dataset sources
fernandes_datasets = {
    'dataset_name':'problema',
    'file_name':'runs.csv',
    'clean_string':'_',
    'delim_char':','
}

db_field_size = {
    'outlet_name':400,
    'volume':100,
    'abstract':6000,
    'keyword':90,
    'model_name':50
}
outlet_type_dict = {
    'Journal article(JA)':'J',
    'conference article (CA)':'C',
    'Article in Press':'J',
    'J':'J',
    'B':'B',
    'S':'S'
}

keyword_type_dict = {
    'authors':'A',
    'source':'S',
    'team_break':'T',  #breaking the keyword into multiple words
    'team_dictionary_application':'P',  #adding a problem dictionary word
    'team_dictionary_algorithm':'M' #adding an algorithm (model) word
}