#!/usr/bin/env python
# -*- coding: utf-8 -*-
import psycopg2
import os, sys, glob
import darpadbconfig as dbconf
import csv
import re
import bs4 as BeautifulSoup
import kb_utils as kbu
import urllib2

#configuration per source
#path = "/home/ise/darpa/data/Engineering_Vilage/EngineeringVillage"
path = "/home/ise/work/EngineeringVillage"
data_path = "/home/ise/work/data"
#path = "C:\Asi\BD\Lior\Literature\DARPA\Docs"
#lang_path = "C:\Asi\BD\Lior\Code\darpa\darpa\\"
lang_path = "/home/ise/work/EngineeringVillage/"
apps_file = "ApplicationsList.txt"
algo_file = "AlgorithmListC.txt"
keywords_file ="res.txt"
add_primitives_flag = False
add_models = False
add_datasets_flag = False
add_runs_flag = False
add_top5_flag = False
add_datasets_title_flag = False
update_datasets_title_flag = False

def levenshteinDistance(s1, s2):
    if len(s1) > len(s2):
        s1, s2 = s2, s1

    distances = range(len(s1) + 1)
    for i2, c2 in enumerate(s2):
        distances_ = [i2+1]
        for i1, c1 in enumerate(s1):
            if c1 == c2:
                distances_.append(distances[i1])
            else:
                distances_.append(1 + min((distances[i1], distances[i1 + 1], distances_[-1])))
        distances = distances_
    return distances[-1]

def find_match(lang_arr,word):
    match = []
    max_distance = 4
    for lang_arr_item in lang_arr:
        distance = levenshteinDistance(lang_arr_item,word)
        if (distance < max_distance):
            if (len(lang_arr_item)>=(3*distance)):
                match.append(lang_arr_item)
    return match

def find_lang_test(lang_arr,file):
    match = []
    with open(file, "r") as f:
        text = f.readlines()
    for word in text:
        word.replace('\n','')
        match = find_match(lang_arr,word)
        if (len(match)>0):
            for item in match:
                print "match: ",word,item
    return

def find_lang(lang_arr,keyword):
    match = find_match(lang_arr,keyword)
    return match

def is_int(s):
      if s.isdigit():
         return s
      return '0'

def test_db(cur,conn):
    sql = """SELECT * from sources;"""
    cur.execute(sql)
    res = cur.fetchone()
    print(res)
    return

def add_source_2db(conf,cur,conn):
    sql = """INSERT INTO sources (source_name) VALUES(%s) ON CONFLICT (source_name) DO NOTHING;"""
    source_id_val = -1
    try:
        cur.execute(sql, (conf['source_name'],))
        conn.commit()
        sql = """SELECT source_id FROM sources WHERE source_name = %s;"""
        cur.execute(sql, (conf['source_name'],))
        source_id_val = cur.fetchone()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error,"exception 1",source_id_val)
        return

    return source_id_val[0]

def init_db():
    try:
        # conn = psycopg2.connect(dbname='darpa', user='yoochose', host='localhost', password='1234')
        conn = psycopg2.connect(dbname='ise', user='ise', host='localhost', password='ise')
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
        return[None,None]

    return([conn,cur])


def get_primitives_algos(conn,cur):
    primitives_list = []
    sql = """SELECT primitive_normal_name,primitive_normal_name2 FROM primitives;"""
    try:
        cur.execute(sql)
        primitives = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "get primitives algos")
        return

    for primitive in primitives:
        primitives_list.append(primitive[0])
        primitives_list.append(primitive[1])

    return(primitives_list)

def add_keywords_primitives_map(conf,conn,cur):

    sql = """INSERT INTO keywords_primitives_map (primitive_name,keyword_name)  VALUES(%s,%s) ON CONFLICT (primitive_name,keyword_name) DO NOTHING;"""
    with open(data_path+"/keyword_primitives_mapping3.csv") as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        headers = next(reader)
        id_idx = 0
        algo_name_idx = 1
        in_voc_idx = 2
        primitives_list_idx = 3
        for row in reader:
            in_voc_val  = int(row[in_voc_idx])
            if in_voc_val > 0:
                keyword_name = row[algo_name_idx]
                primitive_list_val = row[primitives_list_idx]
                primitives = primitive_list_val.split(';')
                for primitive in primitives:
                    try:
                        cur.execute(sql, (primitive,keyword_name,))
                        conn.commit()
                    except (Exception, psycopg2.DatabaseError) as error:
                        print(error, "keywords primitives map",primitive,keyword_name)
                        return



def add_primitives(conf,conn,cur):

    sql = """INSERT INTO primitives (primitive_name,primitive_normal_name,primitive_normal_name2,description,model_family) VALUES(%s,%s,%s,%s,%s) ON CONFLICT (primitive_name) DO NOTHING;"""

    with open(data_path+"/"+conf['file_name']) as csvfile:
        reader = csv.reader(csvfile, delimiter=conf['delim_char'])
        headers = next(reader)
        primitive_name_idx = headers.index(conf['primitive_name'])
        primitive_normalize_name_idx = headers.index('normal')
        primitive_normalize_name2_idx = headers.index('normal2')
        primitive_description_idx = headers.index('description')
        primitive_model_family_idx = headers.index('family')
        #loop on file (can be db)
        for row in reader:
            primitive_name = row[primitive_name_idx]
            primitive_norm = core_normalization(row[primitive_normalize_name_idx])
            primitive_norm2 = core_normalization(row[primitive_normalize_name2_idx])
            primitive_desc = row[primitive_description_idx]
            primitive_family = core_normalization(row[primitive_model_family_idx])
            try:
                cur.execute(sql, (primitive_name,primitive_norm,primitive_norm2,primitive_desc,primitive_family,))
                conn.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error, "exception generate primitives",primitive_name)
                return

def add_runs(conf,conn,cur):
    """
    Add runs resutls from source to db
    """
    primitives = {}
    with open(data_path + "/" + conf['file_name']) as csvfile:
        reader = csv.reader(csvfile, delimiter=conf['delim_char'])
        headers = next(reader)
        first_word = True
        for word in headers:
            if first_word:
                first_word = False
            else:
                primitives[word] = headers.index(word)
        # loop on file (can be db)
        sql = """INSERT INTO runs (dataset_id,primitive_id,accuracy,dataset_name,primitive_name) VALUES(%s,%s,%s,%s,%s) ON CONFLICT (dataset_id,primitive_id) DO NOTHING;"""
        sql_select_dataset = """SELECT dataset_id  FROM datasets WHERE dataset_name = %s; """
        sql_select_primitive = """SELECT primitive_id  FROM primitives WHERE primitive_name = %s; """

        for row in reader:
            dataset_name = row[0]
            # if dataset_name.find('trisopterus_nucleus')<0:
            #     continue
            print("found trisopterus_nucleus ")
            for key, value in primitives.iteritems():
                primitive_name = key
                run_accruacy = kbu.to_number(row[value])
                try:
                    cur.execute(sql_select_dataset, (dataset_name, ))
                    dataset_id = cur.fetchone()
                except (Exception, psycopg2.DatabaseError) as error:
                    print(error, "exception generate runs, select dataset", dataset_name)
                    break

                try:
                    cur.execute(sql_select_primitive, (primitive_name,))
                    primitive_id = cur.fetchone()
                except (Exception, psycopg2.DatabaseError) as error:
                    print(error, "exception generate runs, select primitive", primitive_name)
                    break


                try:
                    cur.execute(sql, (dataset_id, primitive_id,run_accruacy,dataset_name,primitive_name,))
                    conn.commit()
                except (Exception, psycopg2.DatabaseError) as error:
                    print(error, "exception generate datasets, insert run", dataset_name,primitive_name,run_accruacy)
                    break

def add_top_5_primitives_to_datasets(conn,cur):
    """
    Add top 5 primitives per dataset name and id
    """
    sql_select_dataset = """SELECT dataset_id  FROM datasets; """
    #sql_select_runs = """SELECT accuracy,primitive_name,primitive_id  FROM runs WHERE dataset_id = %s ORDER BY accuracy DESC; """
    sql_select_runs = """SELECT t1.accuracy,t1.primitive_name,t1.primitive_id,t2.primitive_normal_name,t2.primitive_normal_name2,t2.model_family  FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE dataset_id = %s ORDER BY accuracy DESC; """
    sql_update_dataset = """UPDATE datasets SET top1_accuracy = %s, top2_accuracy = %s, top3_accuracy = %s, top4_accuracy = %s, top5_accuracy = %s WHERE dataset_id = %s; """
    sql_update_dataset_dsname = """UPDATE datasets SET top1_primitive_name = %s, top2_primitive_name = %s, top3_primitive_name = %s, top4_primitive_name = %s, top5_primitive_name = %s WHERE dataset_id = %s; """
    sql_update_dataset_dsid = """UPDATE datasets SET top1_primitive_id = %s, top2_primitive_id = %s, top3_primitive_id = %s, top4_primitive_id = %s, top5_primitive_id = %s WHERE dataset_id = %s; """
    sql_update_dataset_normal = """UPDATE datasets SET top1_primitive_normal_name = %s, top2_primitive_normal_name = %s, top3_primitive_normal_name = %s, top4_primitive_normal_name = %s, top5_primitive_normal_name = %s WHERE dataset_id = %s; """
    sql_update_dataset_normal2 = """UPDATE datasets SET top1_primitive_normal_name2 = %s, top2_primitive_normal_name2 = %s, top3_primitive_normal_name2 = %s, top4_primitive_normal_name2 = %s, top5_primitive_normal_name2 = %s WHERE dataset_id = %s; """
    sql_update_family = """UPDATE datasets SET top1_primitive_family = %s, top2_primitive_family = %s, top3_primitive_family = %s, top4_primitive_family = %s, top5_primitive_family = %s WHERE dataset_id = %s; """

    try:
        cur.execute(sql_select_dataset)
        datasets = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "exception top5, select dtitleatasets")
        return

    #patch to fix the oocytes_trisopterus_nucleus_2f dataset
    # sql_select_dataset_patch = """SELECT dataset_id FROM datasets WHERE dataset_name=%s;"""
    # patch_name = 'oocytes_trisopterus_nucleus_2f'
    # cur.execute(sql_select_dataset_patch,(patch_name,))
    # datasets = cur.fetchone()


    for dataset in datasets:
        try:
            cur.execute(sql_select_runs,(dataset,))
            primitives = cur.fetchall()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception top5, select primitives",dataset)
            return

        try:
            cur.execute(sql_update_dataset,(primitives[0][0],primitives[1][0],primitives[2][0],primitives[3][0],primitives[4][0],dataset,))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception top5, update accuracy", dataset)
            return

        try:
            cur.execute(sql_update_dataset_dsname,(primitives[0][1], primitives[1][1], primitives[2][1], primitives[3][1], primitives[4][1],dataset,))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception top5, update name", dataset)
            return

        try:
            cur.execute(sql_update_dataset_dsid,(primitives[0][2], primitives[1][2], primitives[2][2], primitives[3][2], primitives[4][2],dataset,))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception top5, update id", dataset)
            return

        try:
            cur.execute(sql_update_dataset_normal, (
            primitives[0][3], primitives[1][3], primitives[2][3], primitives[3][3], primitives[4][3], dataset,))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception top5, update normal name", dataset)
            return

        try:
            cur.execute(sql_update_dataset_normal2, (
            primitives[0][4], primitives[1][4], primitives[2][4], primitives[3][4], primitives[4][4], dataset,))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception top5, update normal name2 ", dataset)
            return

        try:
            cur.execute(sql_update_family, (
            primitives[0][5], primitives[1][5], primitives[2][5], primitives[3][5], primitives[4][5], dataset,))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception top5, family ", dataset)
            return

def _get_title_dict():
    dict = {}
    fname = data_path+'/uci_scrapr.csv'
    with open(fname) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        headers = next(reader)
        title_idx = headers.index('title')
        normal_name_idx = headers.index('normal_name')
        abstract_idx = headers.index('abstract')
        # loop on file (can be db)
        for row in reader:
            title_val = str(row[title_idx])
            normal_name_val = row[normal_name_idx]
            normal_name_val = kbu.core_normalization(normal_name_val)
            abstract_val = str(row[abstract_idx])
            if len(normal_name_val)>2:
                title_val = title_val.lower().replace('(','').replace(')','').replace('\r','').replace('\n','')
                abstract_val = abstract_val.lower().replace('(', '').replace(')', '').replace('\r','').replace('\n','')
                title_new = title_val+abstract_val
                dict[normal_name_val] = title_new

    return dict

def _fix_title_values(cur,conn):
    title_dict = _get_title_dict()
    sql_get_dataset_id = """ SELECT dataset_id FROM datasets WHERE dataset_normal = %s"""
    sql_update_title = """UPDATE datasets SET title = %s WHERE dataset_id = %s; """

    for key, value in title_dict.iteritems():
        try:
            cur.execute(sql_get_dataset_id,(key,))
            dataset_id_val = cur.fetchone()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "fix title get dataset id",key,value)
            continue

        try:
            cur.execute(sql_update_title,(value,dataset_id_val[0],))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "update title fix", key, value)
            continue

def update_empty_title(cur,conn):
    """
    This function update dataset title according to dataset title and abstract fields
    """
    sql_empty_title = """ SELECT dataset_id,dataset_normal FROM datasets WHERE title IS NULL;"""
    sql_update_title = """UPDATE datasets SET title = %s WHERE dataset_id = %s; """

    title_dict = _get_title_dict()

    try:
        cur.execute(sql_empty_title)
        empty_title_datasets = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "exception empty title")
        return

    for dataset in empty_title_datasets:
        if str(dataset[1]) in title_dict:
            try:
                cur.execute(sql_update_title, (title_dict[str(dataset[1])],str(dataset[0]),))
                conn.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error, "update title ",str(dataset[1]),str(dataset[0]),len(title_dict[str(dataset[1])]),title_dict[str(dataset[1])])
                return


def scrap_uci_abstract(conf):


    # actual_link = 'https://archive.ics.uci.edu/ml/datasets/Abalone'
    # link_page = urllib2.urlopen(actual_link)
    # link_soup = BeautifulSoup(link_page,'html')
    # print(link_soup.prettify())

    dataset_page = 'https://archive.ics.uci.edu/ml/datasets.html'
    page = urllib2.urlopen(dataset_page)
    soup = BeautifulSoup(page, 'html')

    bring_link = True
    lines = []
    for link in soup.findAll('a'):
        if re.search(re.escape('datasets/'), str(link)) and not re.search(re.escape('img'), str(link)):
            # print(link)
            actual_link = 'https://archive.ics.uci.edu/ml/' + link['href']
            name_idx = link['href'].find('datasets')
            name_str = link['href'][name_idx + 9:]
            name_str = name_str.replace("+", "_")
            name_str = re.sub(r"%..", "", str(name_str))
            # print(actual_link)
            if bring_link:
                link_page = urllib2.urlopen(actual_link)
                link_soup = BeautifulSoup(link_page, 'html')
                # print(link_soup.prettify())
                abstract_start = str(link_soup).find('Abstract')
                if abstract_start is None:
                    print('none')
                abstract_end = str(link_soup).find('p>', abstract_start)
                abstract_val = str(link_soup)[abstract_start + 13:abstract_end - 2]

                title_soup = link_soup.find('title')
                start_title = str(title_soup).find(':')
                end_title = str(title_soup).find('Data Set')
                title_val = str(title_soup)[start_title + 1:end_title]
                print("title ", title_val)
                print("abstract ", abstract_val)
                lines.append([name_str,title_val, abstract_val])
    with open(conf.data_path+'/'+"uci_scrap.csv", "wb") as f:
        writer = csv.writer(f)
        writer.writerows(lines)

def add_datasets(conf,conn,cur):
    """
    Task: read datasets from source and add to db
    """

    sql = """INSERT INTO datasets (dataset_name,dataset_normal) VALUES(%s,%s) ON CONFLICT (dataset_name) DO NOTHING;"""

    with open(data_path+"/"+conf['file_name']) as csvfile:
        reader = csv.reader(csvfile, delimiter=conf['delim_char'])
        headers = next(reader)
        dataset_name_idx = headers.index(conf['dataset_name'])
        #loop on file (can be db)
        for row in reader:
            dataset_name = row[dataset_name_idx]
            dataset_norm = core_normalization(dataset_name)
            try:
                cur.execute(sql, (dataset_name,dataset_norm,))
                conn.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error, "exception generate datasets",dataset_name)
                return


def generate_models_table(input_file,cur,conn,conf):
    sql = """INSERT INTO models (model_name) VALUES(%s) ON CONFLICT (model_name) DO NOTHING;"""
    with open(input_file, "r") as f:
        text = f.readlines()
    for line in text:
        line.replace('\n', '')
        line = core_normalization(line)
        if (len(line) > conf.db_field_size['model_name']):
            print('model name is too long',len(line))
            continue
        try:
            cur.execute(sql, (line,))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception generate models", line)
            return


def add_dictionary_keywords(n_keyword):
    #place holder for Hadar to add matches with dictionary problems/datasets/primitives
    dict_keywords = []
    return dict_keywords

def core_normalization(keyword_in):
    keyword_in = keyword_in.lower()
    keyword_in = keyword_in.replace('\r','')
    keyword_in = keyword_in.replace('\n','')
    keyword_in = keyword_in.replace('–','_')
    keyword_in = keyword_in.replace('-', '_')
    keyword_in = keyword_in.replace(' ','_')
    return keyword_in

def normalize_keywords(keywords_raw,keywords_delimiter,keywords_type,apps_lang,algos_lang,with_edit_distance):
    keywords_lower = keywords_raw.lower()
    if keywords_delimiter != ',':
        keywords_lower = keywords_lower.replace(',',keywords_delimiter)
    if keywords_delimiter != '/':
        keywords_lower = keywords_lower.replace('/', keywords_delimiter)
    keywords = keywords_lower.split(keywords_delimiter)
    normalized_keywords = []
    for keyword in keywords:
        keyword = keyword.strip()
        n_keyword = keyword.replace(' ','_')
        normalized_keywords.append({'word':n_keyword,'type':keywords_type})  #keyword with multiple words,spaces are replaced with _
        if (with_edit_distance):
            match = find_lang(apps_lang,n_keyword)
            match.extend(find_lang(algos_lang,n_keyword))
            for item in match:
                normalized_keywords.append({'word':item,'type':dbconf.keyword_type_dict['team_dictionary']})
        n_keyword_list = n_keyword.split('_')
        if len(n_keyword_list)>1:  #no need to add a keyword which already exists
            for nkeyword in n_keyword_list:
                normalized_keywords.append({'word':nkeyword,'type':dbconf.keyword_type_dict['team_break']})
    return normalized_keywords

def add_keywords(row,keywords_idx,keywords_delimiter,paper_id_val,keyword_type,cur,conn,keyword_size,apps_lan,algos_lan,with_edit_distance):
    keywords_raw = row[keywords_idx] if keywords_idx > -1 else ''
    keywords_raw_list = normalize_keywords(keywords_raw,keywords_delimiter,keyword_type,apps_lan,algos_lan,with_edit_distance)
    for keyword in keywords_raw_list:
        if len(keyword['word'])>keyword_size:
            print("long keyword ",len(keyword['word']),keyword['word'])
            keyword['word'] = keyword['word'][:keyword_size]
            #continue
        try:
            sql = """INSERT INTO keywords (normalized_keyword_string) VALUES(%s) ON CONFLICT (normalized_keyword_string) DO NOTHING ; """
            cur.execute(sql,(keyword['word'],))
            sql = """SELECT keyword_id FROM keywords WHERE normalized_keyword_string = %s; """
            cur.execute(sql, (keyword['word'],))
            keyword_id_val = cur.fetchone()
            if keyword_id_val is None:
                print("error adding keywords ",keywords_raw_list,keyword)
                return True
            sql = """INSERT INTO papers_keywords (paper_id,keyword_id,keyword_type) VALUES(%s,%s,%s) ON CONFLICT (paper_id,keyword_id) DO NOTHING ; """
            cur.execute(sql,(paper_id_val,keyword_id_val[0],keyword['type'],))
            conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception keyword")
            return True

    return False

def insert_file_2db(fname,conf,cur,conn,source_id_val,apps_lang,algos_lang,with_edit_distance,to_print=False):
    max = 0
    outlet_name_size = dbconf.db_field_size['outlet_name']
    volume_size = dbconf.db_field_size['volume']
    abstract_size = dbconf.db_field_size['abstract']
    keyword_size = dbconf.db_field_size['keyword']
    author_keywords_delimiter = conf['author_keyword_delimiter']
    source_keywords_delimiter = conf['source_keyword_delimiter']
    if conf['format'] == 'csv':
        with open(fname) as csvfile:
            reader = csv.reader(csvfile, delimiter=conf['delim_char'])
            headers = next(reader)
            title_idx = headers.index(conf['title']) if conf['title'] in headers else -1
            outlet_name_idx = headers.index(conf['outlet_name']) if conf['outlet_name'] in headers else -1
            type_idx = headers.index(conf['type']) if conf['type'] in headers else -1
            issn_idx = headers.index(conf['issn']) if conf['issn'] in headers else -1
            eissn_idx = headers.index(conf['eissn']) if conf['eissn'] in headers else -1
            isbn_idx = headers.index(conf['isbn']) if conf['isbn'] in headers else -1
            doi_idx = headers.index(conf['doi']) if conf['doi'] in headers else -1
            pages_idx = headers.index(conf['pages']) if conf['pages'] in headers else -1
            year_idx = headers.index(conf['year']) if conf['year'] in headers else -1
            volume_idx = headers.index(conf['volume']) if conf['volume'] in headers else -1
            number_idx = headers.index(conf['number']) if conf['number'] in headers else -1
            fields_idx = headers.index(conf['fields']) if conf['fields'] in headers else -1
            keywordsa_idx = headers.index(conf['keywords_authors']) if conf['keywords_authors'] in headers else -1
            keywordss_idx = headers.index(conf['keywords_source']) if conf['keywords_source'] in headers else -1
            abstract_idx = headers.index(conf['abstract']) if conf['abstract'] in headers else -1
            reference_no_idx = headers.index(conf['reference_no']) if conf['reference_no'] in headers else -1
            #print(outlet_name_idx,type_idx, issn_idx, eissn_idx, isbn_idx,reference_no_idx)
            Stop = False
            x = 0
            for row in reader:
                    #print("row ",row)
                    doi_val = row[doi_idx] if doi_idx > -1 else ''
                    title_val = row[title_idx] if title_idx > -1 else ''
                    year_val = row[year_idx] if year_idx > -1 else '0'
                    if year_val is '':
                        year_val = '0'
                    year_val = is_int(year_val)
                    volume_val = row[volume_idx] if volume_idx > -1 else ''
                    if len(volume_val)>volume_size:
                        volume_val = ''
                    number_val = row[number_idx] if number_idx > -1 else ''
                    pages_val = row[pages_idx] if pages_idx > -1 else ''
                    outlet_name_val = row[outlet_name_idx] if outlet_name_idx > -1 else ''
                    if len(outlet_name_val)>outlet_name_size:
                        outlet_name_val = outlet_name_val[:outlet_name_size]
                        #print("len ",len(outlet_name_val),outlet_name_val)
                    issn_val = row[issn_idx] if issn_idx > -1 else ''
                    eissn_val = row[eissn_idx] if eissn_idx > -1 else ''
                    isbn_val = row[isbn_idx] if isbn_idx > -1 else ''
                    outlet_type_raw = row[type_idx] if type_idx > -1 else ''
                    abstract_val = row[abstract_idx] if abstract_idx > -1 else ''
                    reference_no_val = row[reference_no_idx] if reference_no_idx > -1 else '0'
                    if reference_no_val is '':
                        reference_no_val='0'
                    reference_no_val = is_int(reference_no_val)
                    if len(abstract_val)>abstract_size:
                        print("abstract too long ",abstract_val,len(abstract_val))
                        abstract_val=abstract_val[:abstract_size]
                    if outlet_type_raw is not '':
                        outlet_type = dbconf.outlet_type_dict.get(outlet_type_raw,'')
                    else:
                        outlet_type = ''
                    if Stop:
                        break
                    # x = x+1
                    # if (x>3000):
                    #     Stop = True
                    sql = """INSERT INTO outlet (outlet_name,type,issn,eissn,isbn) VALUES(%s,%s,%s,%s,%s) ON CONFLICT (outlet_name) DO NOTHING; """
                    try:
                        cur.execute(sql, (outlet_name_val, outlet_type, issn_val, eissn_val, isbn_val,))
                        conn.commit()
                        sql = """SELECT outlet_id FROM outlet WHERE outlet_name = %s; """
                        cur.execute(sql, (outlet_name_val,))
                        outlet_id_val = cur.fetchone()
                    except (Exception, psycopg2.DatabaseError) as error:
                        print(error, "exception 12","outlet_name_val")
                        break

                    if outlet_id_val is None:
                        continue

                    outlet_id_val = outlet_id_val[0]


                    try:
                            sql = """SELECT paper_id FROM papers WHERE title = %s; """
                            cur.execute(sql, (title_val,))
                            paper_id_val = cur.fetchone()
                            if paper_id_val is None:
                                sql = """INSERT INTO papers (doi,source_id,title,year,outlet_id,volume,pages,reference_no) VALUES(%s,%s,%s,%s,%s,%s,%s,%s) ON CONFLICT (title,doi) DO NOTHING ; """
                                cur.execute(sql,(doi_val,source_id_val,title_val,year_val,outlet_id_val,volume_val,pages_val,reference_no_val,))
                                # conn.commit()
                                sql = """SELECT paper_id FROM papers WHERE title = %s; """
                                cur.execute(sql, (title_val,))
                                paper_id_val = cur.fetchone()
                                conn.commit()
                    except (Exception, psycopg2.DatabaseError) as error:
                            print(error, "exception 14")
                            break


                    if paper_id_val is None:
                        print("paper id val is None")
                        continue

                    paper_id_val = paper_id_val[0]
                    try:
                        sql = """INSERT INTO abstract (paper_id,abstract) VALUES(%s,%s) ON CONFLICT (paper_id) DO NOTHING ; """
                        cur.execute(sql, (paper_id_val,abstract_val,))
                        conn.commit()
                    except (Exception, psycopg2.DatabaseError) as error:
                        print(error, "exception 15")
                        break

                    Stop = add_keywords(row,keywordsa_idx,author_keywords_delimiter,paper_id_val,dbconf.keyword_type_dict['authors'],cur,conn,keyword_size,apps_lang,algos_lang,with_edit_distance)
                    if Stop:
                        break
                    Stop = add_keywords(row,keywordss_idx,source_keywords_delimiter,paper_id_val,dbconf.keyword_type_dict['source'], cur, conn,keyword_size,apps_lang,algos_lang,with_edit_distance)
                    if Stop:
                        break

    return

def update_dict(mdict,type,cur,conn):
    sql = """SELECT keyword_id FROM keywords WHERE %s LIKE normalized_keyword_string; """
    sql_update = """UPDATE keywords SET keyword_type = %s WHERE keyword_id = %s; """
    for item in mdict:
        try:
            cur.execute(sql, (item,))
            keyword_id_val = cur.fetchone()
            if keyword_id_val is not None:
                cur.execute(sql_update, (type,keyword_id_val,))
                conn.commit()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "exception keyword doesn't exist",item)
            return True

    return



def insert_dataset_title_2db(file,fname, conf, cur, conn):
    with open(fname,'r') as f:
        sql = """UPDATE datasets SET title = %s WHERE dataset_name = %s; """
        lines = f.readlines()
        found = False
        for line in lines:
            if found:
                break
            lower_line = line.lower()
            if lower_line.find('title') > -1:
                title = lower_line[lower_line.index(':')+len(':'):]
                title = re.sub('[:]', '', title)
                title = title.strip()
                title = title.replace('databases','')
                title = title.replace('database', '')
                title = title.replace('dataset', '')
                title = title.replace('data set', '')
                title = title.replace('data', '')
                found = True
                print("file",file,title,lower_line)
                try:
                    cur.execute(sql, (title,file,))
                    conn.commit()
                except (Exception, psycopg2.DatabaseError) as error:
                    print(error, "add dataset title", file)
                    return

        manual = {}
        manual["abalone"] = "Predict the age of abalone from physical measurements"
        manual["adult"]="Predict whether income exceeds $50K/yr based on census data. Also known as Census Income dataset"
        manual["statlog-image"]= "This dataset is an image segmentation database similar to a database already present in the repository (Image segmentation database) but in a slightly different form"
        manual["flags"] = "This data file contains details of various nations and their flags. With this data you can try things like predicting the religion of a country from its size and the colours in its flag"
        manual["balloons"] = "balloons. The influence of priori_knowledge on concept_acquisition: Experimental and computational results."
        manual["lung-cancer"] = "Lung cancer Optimal Discriminant Plane for a Small Number of Samples and Design Method of Classifier on the Plane"
        manual["hepatitis"] = "Hepatitis Domain Mostly Boolean or numeric-valued attribute types; Includes cost data"
        manual["monks-1"] = "monks problem. A set of three artificial domains over the same attribute space Used to test a wide range of induction algorithms"
        manual["monks-3"] = "monks problem. A set of three artificial domains over the same attribute space Used to test a wide range of induction algorithms"
        manual["chess-krvk"] = "Chess King-Rook vs. King-Knight). Knight Pin Chess End-Game Database Creator"
        manual["heart-va"] = "Heart Disease. The goal field refers to the presence of heart disease in the patient"
        manual["oocytes_merluccius_states_2f"] = "oocytes merluccius states fecundity estimation for fish species Merluccius according to the stage of development of the oocyte"
        manual["planning"]="Planning relax. The dataset concerns with the classification of two mental stages from recorded EEG signals: Planning (during imagination of motor act) and Relax state."
        manual["spambase"]="Classifying Email as Spam or Non-Spam"
        manual["statlog-heart"]="statlog heart. heart disease database"
        manual["statlog-shuttle"] ="statlog shuttle"
        manual["vertebral-column-3clases"] = 'Vertebral Column.Data set containing values for six biomechanical features used to classify orthopaedic patients into 3 classes normal, disk hernia or spondilolysthesis or 2 classes normal or abnormal'
        manual["oocytes_merluccius_nucleus_4d"] = "oocytes merluccius states fecundity estimation for fish species Merluccius according to presence/absence of oocyte nucleus"
        manual["titanic"] = "Info on passengers on the Titanic and whether they survived"
        manual["oocytes_trisopterus_nucleus_2f"]="oocytes trisopterus states fecundity estimation for fish species trisopterus according to presence/absence of oocyte nucleus"
        manual["oocytes_trisopterus_states_5b"]="oocytes merluccius states fecundity estimation for fish species trisopterus according to the stage of development  of the  oocyte"
        manual["plant-texture"]= "One hundred plant species leaves data set Plant Leaf Classification Using Probabilistic Integration of Shape, Texture and Margin Features"
        manual["plant-shape"] = "One hundred plant species leaves data set Plant Leaf Classification Using Probabilistic Integration of Shape, Texture and Margin Features"
        manual["hill-valley"]="Each record represents 100 points on a two-dimensional graph. When plotted in order as the Y co-ordinate, the points will create either a Hill (a bump in the terrain) or a Valley (a dip in the terrain)."
        manual["hayes-roth"] = "hayes roth Topic human subjects study Human subjects classification and recognition"
        manual["twonorm"] = "two class data. Each class is drawn from a multivariate normal distribution with unit covariance matrix"
        manual["zoo"]='zoo Artificial, seven classes of animals'
        manual["low-res-spect"]='The Infra Red Astronomy Satellite IRAS was the first attempt to map the full sky at infra-red wavelengths. Part of the IRAS Low Resolution Spectrometer Database'
        manual["wine-quality-red"]='wine quality Modeling wine preferences by data mining from physicochemical properties'
        manual["teaching"]='Teaching Assistant Evaluation. teaching_quality evaluations  The scores were divided into 3 roughly equal-sized categories'
        manual["pittsburg-bridges-TYPE"]='Pittsburgh Bridges Data Set Bridges database that has original and numeric-discretized datasets'
        manual["wine-quality-white"] = 'wine quality Modeling  by data mining from physicochemical_property'

        for name,title in manual.iteritems():
            title = title.lower()
            try:
                cur.execute(sql, (title,name,))
                conn.commit()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error, "add dataset title manual",name)
                return




def add_dataset_description_to_db(conf,conn,cur):
    start_path = data_path+'/Data'
    for root, dirs, files in os.walk(start_path):
        for file in files:
            if file.endswith('names'):
                fname = os.path.join(root, file)
                #clean_file = file[:file.index('.')]
                clean_file = fname[fname.index('Data')+len('Data')+1:]
                clean_file = clean_file[:clean_file.index('/')]
                insert_dataset_title_2db(clean_file,fname, conf, cur, conn)
                # break
                # break



def add_family_to_keyword(conf,conn,cur):
    sql = """SELECT distinct(primitive_name) as pn FROM keywords_primitives_map; """
    sql_family = """SELECT model_family FROM primitives WHERE primitive_name = %s ;"""
    sql_update = """UPDATE keywords_primitives_map SET model_family = %s WHERE primitive_name = %s; """



    try:
        cur.execute(sql)
        primitives = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "select primitives")
        return True

    if len(primitives) >= 1:
        for primitive in primitives:
            if len(primitive[0])>0:
                print('primtiive',primitive)
                try:
                    cur.execute(sql_family,(str(primitive[0]),))
                    primitive_family = cur.fetchone()
                except (Exception, psycopg2.DatabaseError) as error:
                    print(error, "select primitives family",primitive[0])
                    return True

                if primitive_family is None:
                    print('problem with primitive family None',primitive[0])
                    return

                if len(primitive_family) < 1:
                    print('problem with primitive family',primitive[0])
                    continue

                try:
                    cur.execute(sql_update,(primitive_family[0],primitive[0],))
                    conn.commit()
                except (Exception, psycopg2.DatabaseError) as error:
                    print(error, "update mapping")
                    return True
    else:
        print('problem with primitives')
        return

def main():

    conf = dbconf.fernandes_datasets
    stop = False
    cur = None
    with_edit_distance = False
    update_db = False
    update_dictionary = False
    map_keywords_primitives = False
    update_family_keyword = True

    try:
        #conn = psycopg2.connect(dbname='darpa', user='yoochose', host='localhost', password='1234')
        conn = psycopg2.connect(dbname='ise', user='ise', host='localhost', password='ise')
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
        stop = True
    if map_keywords_primitives:
        add_keywords_primitives_map(conf,conn,cur)

    ##handle fernandes datasets (there are flags for each action.
    ## add datasets title to table
    if add_datasets_title_flag:
        add_dataset_description_to_db(conf,conn,cur)

    if update_family_keyword:
        add_family_to_keyword(conf,conn,cur)

    if update_datasets_title_flag:
         _fix_title_values(cur, conn)
        #update_empty_title(cur, conn)

    ## add primitives to table from Fernandes dataset csv file
    if add_datasets_flag:
        add_datasets(conf, conn, cur)

    ## add primitives to table from Fernandes dataset csv file
    if add_primitives_flag:
        add_primitives(conf,conn,cur)

    ## add models to db from algorithms.txt file
    if add_models:
        generate_models_table(lang_path+algo_file,cur,conn,dbconf)

    if add_runs_flag:
        add_runs(conf,conn,cur)

    if add_top5_flag:
        add_top_5_primitives_to_datasets(conn,cur)

    ### end of Fernandes dataset handling
    stop = True
    #loop on files in folder
    if stop == False:
        source_id = add_source_2db(conf,cur,conn)
        conn.commit()
        if source_id is None:
            sys.exit()
        else:
            apps = generate_lang(lang_path + apps_file)
            algos = generate_lang(lang_path + algo_file)
            print(algos[:10])

            if update_db:
                for root, dirs, files in os.walk(path):
                    for file in files:
                        if file.endswith(conf['format']):
                             fname = os.path.join(root, file)
                             insert_file_2db(fname, conf,cur,conn,source_id,apps,algos,with_edit_distance)
                        #break
                    #break

            if update_dictionary:
                update_dict(apps,dbconf.keyword_type_dict['team_dictionary_application'],cur,conn)
                update_dict(algos,dbconf.keyword_type_dict['team_dictionary_algorithm'],cur,conn)
            conn.commit()
            cur.close()


def test():
    x='Brooks–Iyengar algorithm'
    # x=x.lower()
    # x=x.replace('–','_')
    x = core_normalization(x)
    print(x)
    return

    # x = levenshteinDistance("kitten","biatan")
    # print(x)
    # return

def generate_lang(input_file):
    lang = []
    with open(input_file, "r") as f:
        text = f.readlines()
    for line in text:
        line.replace('\n','')
        line = core_normalization(line)
        lang.append(line)
    return lang

if __name__ == '__main__':
    main()
    #test()

