DELETE FROM papers;
DELETE FROM outlet;
DELETE FROM outlet_fields;
DELETE FROM fields_of_study;
DELETE FROM abstract;
DELETE FROM sources;
DELETE FROM papers_keywords;
DELETE FROM keywords;
