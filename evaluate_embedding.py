import csv
import nltk
import numpy as np
import pandas as pd
import platform
import psycopg2
import string
from nltk.stem.lancaster import LancasterStemmer


from collections import defaultdict
from nltk.corpus import stopwords
from operator import itemgetter

import darpa_createdb as createdb
import kb_utils as kbu

class EvalConfig(object):
    """
      Embedding configuration class
      """
    #path = r'C:\Asi\BD\Lior\Code\darpa\darpa\glove'
    path = '/home/ise/glove/GloVe-1.2'
    description_path = '/home/ise/work/data/Data'
    output_path = '/home/ise/work/out'
    output_base_file="evaluationr"
    vectors_base_file = "vectors"
    vocab_base_file = "vocab"
    glove_pathw = r'C:\Asi\BD\Lior\Code\darpa\darpa\glove'
    out_pathw = r'C:\Asi\BD\Lior\Code\darpa\output'
    max_eval = 5000
    max_algo = 10
    max_problem = 5
    max_family = 17
    conn = None
    cur = None
    algos_lang = None
    evaluation_words = []
    additional_dict = {}
    dataset_source = 'fernandes'
    expected_top = 1   #top expected algos we want to find
    data_algos_top = 3   #number of highest score algos to include
    require_title = True #do we require title for analysis
    #evaluation_words = ['image_classification','text_classification','spam_detection','malware_detection','face_recognition','churn_prediction']
    #evaluation_words = ['random_forests','recurrent_neural_nets','neural_networks','adaboost','svm','support_vector_machines']

    def __init__(self,embedding_size,without_break,with_years):
        self.embedding_size = embedding_size
        self.nb = without_break
        self.nbs = ''
        self.ybs = ''
        if with_years:
            self.ybs = '_years_20_m10'
        if without_break:
            self.nbs = '_nb'
        if platform.platform().lower().find('win') > -1:
            self.path = self.glove_pathw
            self.output_path = self.out_pathw
        self.vectors_file = self.path+"//"+self.vectors_base_file+'_'+str(embedding_size)+self.nbs+self.ybs+".txt"
        self.vocab_file = self.path+"//"+self.vocab_base_file+'_'+str(embedding_size)+self.nbs+self.ybs+".txt"
        self.output_file = self.path+"//"+self.output_base_file+str(embedding_size)+self.nbs+".txt"
        self.windows_platform = False
        self.weights = None
        self.vocab = None
        self.ivocab = None
        self.algos_lang = None

    def set_embedding(self,weights_in,vocab,ivocab):
      """
      Set embedding parameters
      """
      self.weights = weights_in
      self.vocab = vocab
      self.ivocab = ivocab

    def set_language(self, algos_lang):
      """
      Set the list of known algorithm words
      """

      self.algos_lang = algos_lang

    def _set_additional_stopwords(self):
        self.additional_dict = {}
        self.additional_dict['versus'] = 1
        self.additional_dict['data'] = 1
        self.additional_dict['file'] = 1
        self.additional_dict['like'] = 1
        self.additional_dict['predicting'] = 1
        self.additional_dict['size'] = 1
        self.additional_dict['domain'] = 1
        self.additional_dict['attribute'] = 1
        self.additional_dict['boolean'] = 1
        self.additional_dict['classes'] = 1
        self.additional_dict['classify'] = 1
        self.additional_dict['features'] = 1
        self.additional_dict['set'] = 1
        self.additional_dict['used'] = 1
        self.additional_dict['values'] = 1
        self.additional_dict['known'] = 1
        self.additional_dict['database'] = 1
        self.additional_dict['pin'] = 1
        self.additional_dict['first'] = 1
        self.additional_dict['mining'] = 1
        self.additional_dict['dataset'] = 1
        self.additional_dict['uc'] = 1
        self.additional_dict['classifying'] = 1
        self.additional_dict['classification'] = 1
        self.additional_dict['term'] = 1
        self.additional_dict['influence'] = 1
        self.additional_dict['16'] = 1
        self.additional_dict['one'] = 1
        self.additional_dict['values'] = 1
        self.additional_dict['method'] = 1
        self.additional_dict['flanges'] = 1
        self.additional_dict['sets'] = 1
        self.additional_dict['different'] = 1
        self.additional_dict['experiment'] = 1
        self.additional_dict['datasets'] = 1
        self.additional_dict['measurements'] = 1
        self.additional_dict['samples'] = 1
        self.additional_dict['histogram'] = 1
        self.additional_dict['classifier'] = 1
        self.additional_dict['number'] = 1
        self.additional_dict['small'] = 1
        self.additional_dict['discriminant'] = 1
        self.additional_dict['property'] = 1
        self.additional_dict['modeling'] = 1
        self.additional_dict['groups'] = 1
        self.additional_dict['classication'] = 1
        self.additional_dict['classifications'] = 1
        self.additional_dict['classifiers'] = 1
        self.additional_dict['terms'] = 1
        self.additional_dict['class'] = 1
        self.additional_dict['classificator'] = 1
        self.additional_dict['hematite'] = 1
        self.additional_dict['least'] = 1


class AlgoDist(object):
  """
  Algorithms distances from description words class
  """

  algo_num = 0
  nearest_algo_dict = {}

  def __init__(self, conf):
      self.conf = conf
      for algo in self.conf.algos_lang:
          if algo in self.conf.vocab:
            self.nearest_algo_dict[algo] = 0.0


  def update_algo_distance_fernandes(self, word):
      """
      This function updates the distance for each normal primitive and aggregated distance from the overall description
      """

      pred_vec = self.conf.weights[self.conf.vocab[word], :]
      dist = np.dot(self.conf.weights, pred_vec.T)
      for algo in self.conf.algos_lang:
          if algo in self.conf.vocab:
            self.nearest_algo_dict[algo] = self.nearest_algo_dict[algo]+dist[self.conf.vocab[algo]]

  def update_algo_distance(self, word):
      """
      This function updates the list of algorithms and aggregated distance from the overall description
      """

      pred_vec = self.conf.weights[self.conf.vocab[word], :]
      dist = np.dot(self.conf.weights, pred_vec.T)
      dist[self.conf.vocab[word]] = -np.Inf
      sort_dist = np.argsort(-dist)[:self.conf.max_eval]
      for item in sort_dist:
          if self.conf.ivocab[item] in self.conf.algo_lang:
              # if algo_num < conf.max_algo:
              dist[item] = format(dist[item], '.2f')
              if self.conf.ivocab[item] in self.nearest_algo_dict:
                  self.nearest_algo[self.nearest_algo_dict[self.conf.ivocab[item]]]['dist'] = \
                  self.nearest_algo[self.nearest_algo_dict[self.conf.ivocab[item]]]['dist'] + dist[item]
              else:
                  self.nearest_algo.append({'val': self.conf.ivocab[item], 'dist': dist[item]})
                  self.nearest_algo_dict[self.conf.ivocab[item]] = self.algo_num
                  self.algo_num = self.algo_num + 1


def save_dictionary_to_file(fname,dict):
    """
    This function save a dictionary with list value to a txt file key:value1:value2:value3
    """
    with open(fname,'w+') as file:  # rewrite file
        for id, values in dict.items():
            file.write(id)
            for value in values:
                file.write(","+value)
            file.write('\n')

    return

def save_list_to_file(fname,list):
    """
    This function save a dictionary with list value to a txt file key:value1:value2:value3
    """
    with open(fname,'w') as f:  # rewrite file
        for item in list:
            f.write(item[0] + '\n')

    return

def load_dictionary_from_file(fname):
    """
    This function generates a dictionary from txt file key:value1:value2:value3
    """
    dict = {}
    return dict

def get_dataset_normals(conn,cur):
    sql = """SELECT dataset_normal FROM datasets;"""
    try:
        cur.execute(sql)
        datasets = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "select datasets")
        return

    return datasets

def _get_expected_algos_for_dataset(conf,dataset_id):
    sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
    try:
        conf.cur.execute(sql,(dataset_id,))
        datasets = conf.cur.fetchone()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "select datasets")
        return

    return datasets
def _get_primitives_list_for_keyword(conn,cur,algo):
    sql = """SELECT primitive_name FROM primitives WHERE (primitive_normal_name = %s OR primitive_normal_name2 = %s);"""
    try:
        cur.execute(sql,(algo,algo,))
        primitives = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "select primitives for keyword",algo)
        return ''

    primitives_list = []
    for primitive in primitives:
        primitives_list.append(str(primitive[0]))

    primitives_str = ';'.join(primitives_list)
    return primitives_str

def get_list_of_fernandes_model_keywords():
    """
    This function updates the distance for each normal primitive and aggregated distance from the overall description
    """

    if platform.platform().lower().find('win') == -1:
        conn, cur = createdb.init_db()
        embedding_size = 200
        without_break_options = True
        with_years = False
        embedding_conf = _emedding_init(embedding_size, without_break_options, with_years)
        embedding_conf.conn = conn
        embedding_conf.cur = cur
        embedding_conf.set_language(_generate_lang_db(conn, cur))
        cols = ['algo_name','in_voc','primitives_list']
        lines = []
        algo_list = kbu.remove_duplicates_from_list(embedding_conf.algos_lang)
        for algo in algo_list:
            in_voc = 0
            if algo in embedding_conf.vocab:
                in_voc = 1
            primitives = _get_primitives_list_for_keyword(conn,cur,algo)
            line = [algo,in_voc,primitives]
            lines.append(line)

    df = pd.DataFrame(lines, columns=cols)
    df.to_csv(embedding_conf.output_path + '/keyword_primitives_mapping.csv')


def _calc_baseline_score(expected_algos,dataset_id,conf):
    sql = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s or t2.primitive_normal_name2 = %s) ORDER BY accuracy DESC;"""

    try:
        conf.cur.execute(sql, (dataset_id, 'random_forest', 'random_forest',))
        accuracy = conf.cur.fetchone()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "calc baseline score ")
        return -1
    base_accuracy = accuracy[0]
    act_accuracy = expected_algos[4]
    res = kbu.to_number(base_accuracy) / (1.0 * kbu.to_number(act_accuracy))
    return res

def _calc_baseline_score_family(expected_algos,dataset_id,conf):
    sql = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s or t2.primitive_normal_name2 = %s) ORDER BY accuracy DESC;"""
    sql_family = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.model_family = %s) ORDER BY accuracy DESC;"""

    try:
        conf.cur.execute(sql, (dataset_id, 'random_forest', 'random_forest',))
        accuracy = conf.cur.fetchone()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "calc baseline score ")
        return -1
    conf.cur.execute(sql_family, (dataset_id, 'rf',))
    family_accuracy = conf.cur.fetchone()
    base_accuracy = accuracy[0]
    act_accuracy = expected_algos[4]
    family_res = kbu.to_number(family_accuracy[0]) / (1.0 * kbu.to_number(act_accuracy))
    res = kbu.to_number(base_accuracy) / (1.0 * kbu.to_number(act_accuracy))
    return [res, family_res]

def lst2pgarr(alist):
    return  "','".join(alist)

def _calc_MRR2(data_algos,expected_algos,dataset_id,conf):
    #use the keywords to primitives mapping
    sql_max_acc = """SELECT MAX(accuracy) FROM runs WHERE dataset_id = %s;"""
    sql_max_primitives = """SELECT primitive_name FROM runs WHERE dataset_id = %s AND accuracy = %s;"""
    #sql_match_primitive_keyword = """SELECT map_id FROM keywords_primitives_map WHERE keyword_name = %s AND primitive_name IN ('%s');"""
    sql_match_primitive_keyword = """SELECT map_id FROM keywords_primitives_map WHERE keyword_name = %s AND primitive_name = %s;"""
    not_found = True
    kid = 0
    len_algos = len(data_algos)
    MRR = 0
    MRR_res = 0

    try:
        conf.cur.execute(sql_max_acc, (dataset_id,))
        max_accuracy = conf.cur.fetchone()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "max accuracy")
        return 0

    if max_accuracy is None:
        print('error max_accuracy')
        return 0

    #print('debug max accuracy ',max_accuracy[0])

    max_accuracy_val = str(max_accuracy[0])
    try:
        conf.cur.execute(sql_max_primitives, (dataset_id,max_accuracy_val,))
        max_accuracy_primitives = conf.cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "max accuracy primitives")
        return 0

    if max_accuracy_primitives is None:
        print('error max_accuracy primitives')
        return 0

    primitives_str = ''
    pid = 0
    if len(max_accuracy_primitives) < 1:
        print('no primitives')
        return 0
    primitives_list = []
    for max_primitive in max_accuracy_primitives:
        primitives_list.append(max_primitive[0])

    #print('primitives list ',primitives_list)

    #primitives_str = lst2pgarr(primitives_list)
    #primitives_tuple = tuple([primitives_str])


    #print('debug max primitives tuple',primitives_str)
    while not_found and (kid < len_algos):
        #print(data_algos[kid][0])
        for primitive in primitives_list:
            if not_found:
                #print('primitive ',primitive)
                try:
                    conf.cur.execute(sql_match_primitive_keyword,(data_algos[kid][0],primitive,))
                    matched_primitives = conf.cur.fetchall()
                except (Exception, psycopg2.DatabaseError) as error:
                    print(error, "match primitives",data_algos[kid][0],primitive)
                    return 0

                if len(matched_primitives)>0:
                    #print('matched ',matched_primitives[0][0])
                    MRR = kid + 1
                    not_found = False
                    print('matched algo ',data_algos[kid][0],'MRR ',MRR)
        if not_found:
                    kid = kid + 1

    if MRR> 0 and (not not_found):
        MRR_res = 1.0 / (MRR*1.0)
    else:
        MRR_res = 0
        print('problem with MRR')
    return MRR_res

def _calc_MRR_family(data_algos,expected_algos,dataset_id,conf):
    #use the keywords to primitives mapping
    sql_max_acc = """SELECT MAX(accuracy) FROM runs WHERE dataset_id = %s;"""
    sql_max_primitives = """SELECT primitive_name FROM runs WHERE dataset_id = %s AND accuracy = %s;"""
    sql_algo_family = """SELECT model_family FROM keywords_primitives_map WHERE keyword_name = %s;"""

    #sql_match_primitive_keyword = """SELECT map_id FROM keywords_primitives_map WHERE keyword_name = %s AND primitive_name IN ('%s');"""
    sql_match_primitive_keyword = """SELECT map_id FROM keywords_primitives_map WHERE keyword_name = %s AND model_family = %s;"""
    not_found = True
    kid = 0
    len_algos = len(data_algos)
    MRR = 0
    MRR_res = 0

    try:
        conf.cur.execute(sql_max_acc, (dataset_id,))
        max_accuracy = conf.cur.fetchone()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "max accuracy")
        return 0

    if max_accuracy is None:
        print('error max_accuracy')
        return 0

    #print('debug max accuracy ',max_accuracy[0])

    max_accuracy_val = str(max_accuracy[0])
    try:
        conf.cur.execute(sql_max_primitives, (dataset_id,max_accuracy_val,))
        max_accuracy_primitives = conf.cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "max accuracy primitives")
        return 0

    if max_accuracy_primitives is None:
        print('error max_accuracy primitives')
        return 0

    primitives_str = ''
    pid = 0
    if len(max_accuracy_primitives) < 1:
        print('no primitives')
        return 0
    primitives_list = set()
    for max_primitive in max_accuracy_primitives:
        primitives_list.add(max_primitive[0])

    print('primitives list ',primitives_list)

    # primitives_str = lst2pgarr(primitives_list)
    # print('debug str ',primitives_str)

    sql_select_family = """ SELECT distinct(model_family) FROM keywords_primitives_map WHERE primitive_name in ('%s') """ % "','".join(
        primitives_list)

    try:
        conf.cur.execute(sql_select_family)
        matched_families = conf.cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "match primitives")
        return 0

    family_list = []
    for family in matched_families:
        family_list.append(family[0])
    print('family list ',family_list)
    #primitives_tuple = tuple([primitives_str])


    #print('debug max primitives tuple',primitives_str)
    family_dict = {}
    family_id = 0
    while not_found and (kid < len_algos):
        print(kid,data_algos[kid][0])
        try:
            conf.cur.execute(sql_algo_family, (data_algos[kid][0], ))
            algo_family = conf.cur.fetchone()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "algo family")
            return 0
        new_family = False

        print('keyword algo family ',algo_family)
        if len(algo_family)>0:
            if algo_family[0] not in family_dict:
                family_dict[algo_family[0]] = 1
                family_id = family_id + 1
                new_family = True
        else:
            print('no algo family for algo')
            return 0

        if new_family:
            for primitive in family_list:
                if not_found:
                    #print('primitive ',primitive)
                    try:
                        conf.cur.execute(sql_match_primitive_keyword,(data_algos[kid][0],primitive,))
                        matched_primitives = conf.cur.fetchall()
                    except (Exception, psycopg2.DatabaseError) as error:
                        print(error, "match primitives",data_algos[kid][0],primitive)
                        return 0

                    if len(matched_primitives)>0:
                        #print('matched ',matched_primitives[0][0])
                        MRR = family_id
                        not_found = False
                        print('matched algo ',data_algos[kid][0],'MRR ',MRR)
        if not_found:
               kid = kid + 1


    if MRR> 0 and (not not_found):
        MRR_res = 1.0 / (MRR*1.0)
    else:
        MRR_res = 0
        print('problem with MRR')
    return MRR_res

def MRR_family_randomforest():
    if platform.platform().lower().find('win') == -1:
        conn, cur = createdb.init_db()
        embedding_size = 200
        without_break_options = True
        with_years = False
        embedding_conf = _emedding_init(embedding_size, without_break_options, with_years)
        embedding_conf.conn = conn
        embedding_conf.cur = cur
        data = []
        cols = ['dataset_name', 'rf_max_acc']
        num = 0
        avg_MRR = 0
        datasets_ids = _get_dataset_ids(conn, cur)  # return tuples of id and name
        sql = """ SELECT MAX(accuracy) FROM runs WHERE dataset_id = %s;"""
        sql_rfmax = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.model_family = 'rf' and t1.accuracy = %s);"""

        for dataset_id in datasets_ids:
            print("dataset:", dataset_id[0])

            try:
                cur.execute(sql, (dataset_id[0],))
                max_accuracy = cur.fetchone()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error, "random forest MRR no max accuracy for dataset ",dataset_id[0])
                return 0

            max_accuracy_val = str(max_accuracy[0])
            print("max accuracy: ",max_accuracy_val)

            try:
                cur.execute(sql_rfmax, (dataset_id[0],max_accuracy_val,))
                rf_runs = cur.fetchall()
            except (Exception, psycopg2.DatabaseError) as error:
                print(error, "random forest MRR find runs with max ", dataset_id[0],max_accuracy_val)
                return 0

            if rf_runs is None:
                MRR = 0
            else:
                if len(rf_runs)>0:
                    MRR = 1
                else:
                    MRR = 0

            num = num + 1
            avg_MRR = avg_MRR + MRR
            line = [dataset_id[1], MRR]
            data.append(line)
            # writer.writerow(line)

        if num > 0:
            print("score ", (avg_MRR / (1.0 * num)), " num ", num)
            avg_MRR = avg_MRR / (1.0 * num)
            line = ['average', avg_MRR]
            data.append(line)
            # writer.writerow(line)

        df = pd.DataFrame(data, columns=cols)
        df.to_csv(embedding_conf.output_path + '/results_MRR_famrf.csv')


def _calc_MRR(data_algos,expected_algos,dataset_id,conf):
    not_found = True
    id = 0
    len_algos = len(data_algos)
    MRR = 0
    MRR_res = 0
    print(expected_algos[2],expected_algos[3])
    while not_found and (id < len_algos):
        print(data_algos[id][0])
        if (data_algos[id][0] == expected_algos[2] or data_algos[id][0] == expected_algos[3]):
            MRR = id + 1
            not_found = False
        else:
            id = id + 1
    if MRR> 0:
        MRR_res = 1.0 / (MRR*1.0)
        print(MRR,MRR_res)
    else:
        MRR_res = 0
        print('problem with MRR')
    return MRR_res


    #sql = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s or t2.primitive_normal_name2 = %s or t2.primitive_normal_name3 = %s) and (accuracy > 0) ORDER BY accuracy DESC;"""

    #return

def _calc_match_score(data_algos,expected_algos,dataset_id,conf,recall=1):
    # sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
    if recall > 1:
        sql = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s OR t2.primitive_normal_name2 = %s  OR t2.primitive_normal_name = %s OR t2.primitive_normal_name2 = %s) AND (t1.accuracy > 0) ORDER BY t1.accuracy DESC;"""

    if recall == 1:
        sql = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s or t2.primitive_normal_name2 = %s or t2.primitive_normal_name3 = %s) and (accuracy > 0) ORDER BY accuracy DESC;"""

    not_found = True;
    i= 0
    while not_found:
        try:
            if recall > 1:
                data_algo_list = set()
                for i in range(recall):
                    data_algo_list.add(data_algos[i][0])

                print('data algo list: ',data_algo_list)

                sql = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name IN ('%s') OR t2.primitive_normal_name2 IN ('%s') OR t2.primitive_normal_name3 IN ('%s')) AND (t1.accuracy > 0) ORDER BY t1.accuracy DESC""" % (dataset_id,"','".join(
                    data_algo_list),"','".join(data_algo_list),"','".join(data_algo_list))

                #print(dataset_id,data_algos[0][0],data_algos[1][0])
                #conf.cur.execute(sql,(dataset_id,data_algos[0][0],data_algos[0][0],data_algos[1][0],data_algos[1][0],data_algos[2][0],data_algos[2][0],))
                #conf.cur.execute(sql,(dataset_id,data_algos[0][0],data_algos[0][0],data_algos[1][0],data_algos[1][0],))
                conf.cur.execute(sql)
            if recall == 1:
               conf.cur.execute(sql,(dataset_id,data_algos[i][0],data_algos[i][0],data_algos[i][0]))
            accuracy = conf.cur.fetchone()
            if accuracy is not None:
                not_found = False
            else:
                i = i+1
                print('strange... why i is bigger than 0?')
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "calc match score ",data_algos[0][0],data_algos[1][0],recall)
            return -1

    rec_accuracy = accuracy[0]
    act_accuracy = expected_algos[4]
    print("rec accuracy ",rec_accuracy,"act accuracy ",act_accuracy)
    res = kbu.to_number(rec_accuracy)/(1.0*kbu.to_number(act_accuracy))
    return res

def _calc_match_score_family_hyb(data_algos,expected_algos,dataset_id,conf,recall=1):
    """
    data_algos is a list [algo,dist,score_idx]
    """
    # sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
    if recall > 1:
        sql = """SELECT t1.accuracy,t2.model_family,t1.primitive_name FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s OR t2.primitive_normal_name2 = %s  OR t2.primitive_normal_name = %s OR t2.primitive_normal_name2 = %s) AND (t1.accuracy > 0) ORDER BY t1.accuracy DESC;"""

    if recall == 1:
        sql = """SELECT t1.accuracy,t2.model_family,t1.primitive_name FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s or t2.primitive_normal_name2 = %s) and (accuracy > 0) ORDER BY accuracy DESC;"""

    sql_family = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.model_family = %s) AND (t1.accuracy > 0) ORDER BY t1.accuracy DESC;"""


    not_found = True;
    i= 0
    res_list = []
    for algo in data_algos:
        try:
                if recall == 1:
                   conf.cur.execute(sql,(dataset_id,algo[0],algo[0]))
                accuracy = conf.cur.fetchone()
                if accuracy is None:
                    continue
        except (Exception, psycopg2.DatabaseError) as error:
                print(error, "calc match score ",algo[0],algo[1],recall)
                return -1

        conf.cur.execute(sql_family,(dataset_id,accuracy[1]))
        family_accuracy = conf.cur.fetchone()
        rec_accuracy = accuracy[0]
        act_accuracy = expected_algos[4]
        #print("family accuracy ",family_accuracy[0]," rec accuracy ",rec_accuracy,"act accuracy ",act_accuracy)
        res = kbu.to_number(rec_accuracy)/(1.0*kbu.to_number(act_accuracy))
        family_res = kbu.to_number(family_accuracy[0]) / (1.0 * kbu.to_number(act_accuracy))

        res_list.append([expected_algos[0], expected_algos[1],expected_algos[4],str(accuracy[2]),res,str(accuracy[1]),family_res,algo[0],algo[1],algo[2]])

    return res_list

def _calc_match_score_family(data_algos,expected_algos,dataset_id,conf,recall=1):
    # sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
    if recall > 1:
        sql = """SELECT t1.accuracy,t2.model_family,t1.primitive_name FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s OR t2.primitive_normal_name2 = %s  OR t2.primitive_normal_name = %s OR t2.primitive_normal_name2 = %s) AND (t1.accuracy > 0) ORDER BY t1.accuracy DESC;"""

    if recall == 1:
        sql = """SELECT t1.accuracy,t2.model_family,t1.primitive_name FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name = %s or t2.primitive_normal_name2 = %s) and (accuracy > 0) ORDER BY accuracy DESC;"""

    sql_family = """SELECT t1.accuracy FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.model_family = %s) AND (t1.accuracy > 0) ORDER BY t1.accuracy DESC;"""


    not_found = True;
    i= 0
    while not_found:
        try:
            if recall > 1:
                data_algo_list = set()
                for i in range(recall):
                    data_algo_list.add(data_algos[i][0])

                print('data algo list: ',data_algo_list)

                sql = """SELECT t1.accuracy,t2.model_family,t1.primitive_name FROM runs t1 INNER JOIN primitives t2 on t1.primitive_id = t2.primitive_id WHERE t1.dataset_id = %s and (t2.primitive_normal_name IN ('%s') OR t2.primitive_normal_name2 IN ('%s')  OR t2.primitive_normal_name2 IN ('%s')) AND (t1.accuracy > 0) ORDER BY t1.accuracy DESC""" % (dataset_id,"','".join(data_algo_list),"','".join(data_algo_list),"','".join(data_algo_list))

                #print(dataset_id,data_algos[0][0],data_algos[1][0])
                #conf.cur.execute(sql,(dataset_id,data_algos[0][0],data_algos[0][0],data_algos[1][0],data_algos[1][0],data_algos[2][0],data_algos[2][0],))
                #conf.cur.execute(sql,(dataset_id,data_algos[0][0],data_algos[0][0],data_algos[1][0],data_algos[1][0],))
                conf.cur.execute(sql)
            if recall == 1:
               conf.cur.execute(sql,(dataset_id,data_algos[i][0],data_algos[i][0]))
            accuracy = conf.cur.fetchone()
            if accuracy is not None:
                not_found = False
            else:
                i = i+1
                print('strange... why i is bigger than 0?')
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "calc match score ",data_algos[0][0],data_algos[1][0],recall)
            return -1

    conf.cur.execute(sql_family,(dataset_id,accuracy[1]))
    family_accuracy = conf.cur.fetchone()
    rec_accuracy = accuracy[0]
    act_accuracy = expected_algos[4]
    print("family accuracy ",family_accuracy[0]," rec accuracy ",rec_accuracy,"act accuracy ",act_accuracy)
    res = kbu.to_number(rec_accuracy)/(1.0*kbu.to_number(act_accuracy))
    family_res = kbu.to_number(family_accuracy[0]) / (1.0 * kbu.to_number(act_accuracy))
    return [act_accuracy,str(accuracy[2]),res,str(accuracy[1]),family_res]

def _get_dataset_ids(conn,cur):
    sql = """SELECT dataset_id,dataset_name FROM datasets;"""
    try:
        cur.execute(sql)
        datasets = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "select datasets")
        return

    return datasets

def add_avg_accuracy_to_res():
    conn, cur = createdb.init_db()
    embedding_size = 200
    without_break_options = True
    with_years = False
    conf = _emedding_init(embedding_size, without_break_options, with_years)
    conf.conn = conn
    conf.cur = cur
    conf.set_language(_generate_lang_db(conn, cur))
    infile = conf.output_path + "/" + 'results_list_nb_gilad_2611_stem_dictf.csv'
    outfile = conf.output_path + "/" + 'results_list_nb_gilad_2611_stem_dictf2.csv'
    sql = """SELECT AVG(accuracy) FROM runs WHERE (dataset_name = %s AND accuracy > 0);"""
    with open(infile,'rb') as inf:
        lines = inf.readlines()
        with open(outfile,'w') as outf:
            outf.write('id,dataset_name,top_acc,bot_acc,rec_acc,rf_acc,avg_acc'+'\n')
            first = True
            for line in lines:
                if first:
                    first = False
                else:
                    sline = line.strip().split(",")
                    cline = line.strip()
                    try:
                        cur.execute(sql,(sline[1],))
                        avg_val = cur.fetchone()

                    except (Exception, psycopg2.DatabaseError) as error:
                        print(error, "select avg",sline[1])
                        return

                    if avg_val[0] is None:
                        print(sline[1])
                        continue

                    print(avg_val[0])
                    res = kbu.nformat(avg_val[0])
                    outf.write(cline+','+str(res)+'\n')


def _get_min_max_dataset_score(conf,dataset_id):
    sql = """SELECT MAX(accuracy),MIN(accuracy) FROM runs WHERE accuracy > 0 AND dataset_id =%s;"""
    try:
        conf.cur.execute(sql,(dataset_id,))
        dataset = conf.cur.fetchone()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "select datasets")
        return

    return dataset

def MRR_evaluation(family=False):
    """
    This function evaluates the results for Fernandes dataset
    """

    if platform.platform().lower().find('win') == -1:
        conn, cur = createdb.init_db()
        embedding_size = 200
        without_break_options = True
        with_years = False
        embedding_conf = _emedding_init(embedding_size, without_break_options, with_years)
        embedding_conf.conn = conn
        embedding_conf.cur = cur
        embedding_conf.set_language(_generate_lang_db(conn, cur))
        data = []
        cols = ['dataset_name', 'MRR']
        num = 0
        avg_MRR = 0
        datasets_ids = _get_dataset_ids(conn, cur)  # return tuples of id and name
        for dataset_id in datasets_ids:
            print("dataset:", dataset_id[1])
            data_algos = _get_algos_for_dataset(embedding_conf, dataset_id[0],True)  # returns a dictionary of norm form of primitives algo which are closest + distance
            if len(data_algos) < 1:
                print('no_algos_for_dataset')
                continue

            expected_algos = _get_expected_algos_for_dataset(embedding_conf, dataset_id[0])  # returns the norm form or primitives algos suitable for this dataset + accuracy
            # sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
            if family:
                MRR = _calc_MRR_family(data_algos, expected_algos, dataset_id[0], embedding_conf)
            else:
              MRR = _calc_MRR2(data_algos, expected_algos, dataset_id[0], embedding_conf)
            num = num + 1
            avg_MRR = avg_MRR + MRR
            if MRR > 0:
                rank = 1/(1.0*MRR) - 1
                rank = int(rank)
            else:
                rank = -1
            #print('MRR results:  expected', expected_algos[1],expected_algos[5],'actual: ',data_algos[rank],'rank ',str(kbu.nformat(MRR)))
            # line = [dataset_id[1]]  #dataset name
            line = [expected_algos[0],str(kbu.nformat(MRR)) ]
            data.append(line)
            # writer.writerow(line)


        if num > 0:
            print("score ", (avg_MRR / (1.0 * num)), " num ", num)
            avg_MRR = avg_MRR / (1.0*num)
            line = ['average', avg_MRR]
            data.append(line)
            # writer.writerow(line)

        df = pd.DataFrame(data, columns=cols)
        df.to_csv(embedding_conf.output_path + '/results_MRR_fam2.csv')

def generate_data_family():
    """
    This function evaluates the results for Fernandes dataset
    """

    if platform.platform().lower().find('win') == -1:
        conn, cur = createdb.init_db()
        embedding_size = 200
        without_break_options = True
        with_years = False
        embedding_conf = _emedding_init(embedding_size, without_break_options, with_years)
        embedding_conf.conn = conn
        embedding_conf.cur = cur
        embedding_conf.set_language(_generate_lang_db(conn, cur))
        data = []
        datam = []
        cols = ['dataset_name', 'top_primitive','top_accuracy', 'rec_algo','ers_accuracy_ratio','reco_family','ers_family_accuracy_ratio','rf_accuracy_ratio','rf_family_acc_ratio']
        avg_match_score = 0.0
        avg_baseline_score = 0.0
        avg_family_match_score = 0.0
        avg_family_baseline_score = 0.0
        avg_max = 0.0
        avg_min_ratio = 0.0
        num = 0
        recall = 1
        datasets_ids = _get_dataset_ids(conn, cur)  # return tuples of id and name
        # with open(embedding_conf.output_path + '/results_list_nb_gilad_2311_t2.csv', 'a') as f:
        #     writer = csv.writer(f)
        #     writer.writerow(cols)
        for dataset_id in datasets_ids:
            print("dataset:", dataset_id[1])
            data_algos = _get_algos_for_dataset(embedding_conf, dataset_id[0])  # returns a dictionary of norm form of primitives algo which are closest + distance
            if len(data_algos) < 1:
                print('no_algos_for_dataset')
                continue

            expected_algos = _get_expected_algos_for_dataset(embedding_conf, dataset_id[0])  # returns the norm form or primitives algos suitable for this dataset + accuracy
            # sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
            #match_score = _calc_match_score_family(data_algos, expected_algos, dataset_id[0], embedding_conf, recall)
            [act_accuracy,rec_algo, res, rec_family, family_res] = _calc_match_score_family(data_algos, expected_algos, dataset_id[0], embedding_conf, recall)
            [base_score,base_score_fam] = _calc_baseline_score_family(expected_algos, dataset_id[0], embedding_conf)
            #max_score = expected_algos[4]
            min_max_score = _get_min_max_dataset_score(embedding_conf, dataset_id[0])
            min_score = min_max_score[1]
            max_score = min_max_score[0]
            min_score_ratio = kbu.to_number(min_score) / (kbu.to_number(max_score) * 1.0)
            avg_max = avg_max + kbu.to_number(max_score)
            avg_min_ratio = avg_min_ratio + min_score_ratio
            avg_match_score = avg_match_score + res
            avg_family_match_score = avg_family_match_score + family_res
            avg_baseline_score = avg_baseline_score + base_score
            avg_family_baseline_score = avg_family_baseline_score + base_score_fam
            num = num + 1
            print(expected_algos[0], str(family_res), str(base_score), str(min_score_ratio))
            # line = [dataset_id[1]]  #dataset name
            line = [expected_algos[0], expected_algos[1],expected_algos[4], rec_algo, res, rec_family, family_res, base_score,base_score_fam]
            data.append(line)
            # writer.writerow(line)
            # linem = []
            # linem.extend(expected_algos)
            # for item in data_algos:
            #     linem.extend([item[0], item[1]])  # norm primitive name, distance
            # linem.append(match_score)
            # datam.append(linem)

        if num > 0:
            print("score ", (avg_match_score / (1.0 * num)), " base score ", (avg_baseline_score / (1.0 * num)), " num ",num)
            avg_max = avg_max / (1.0 * num)
            avg_match_score = avg_match_score / (1.0 * num)
            avg_baseline_score = avg_baseline_score / (1.0 * num)
            avg_min_ratio = avg_min_ratio / (1.0 * num)
            line = ['average','best_algo', avg_max,'rec_algo',avg_match_score,'rec_family',avg_family_match_score/ (1.0 * num),avg_baseline_score,avg_family_baseline_score/ (1.0 * num)]
            data.append(line)
            # writer.writerow(line)

        df = pd.DataFrame(data, columns=cols)
        df.to_csv(
            embedding_conf.output_path + '/results_family' + str(recall) + '.csv')

def generate_data_family_hybrid():
    """
    This function evaluates the results for Fernandes dataset
    """

    if platform.platform().lower().find('win') == -1:
        conn, cur = createdb.init_db()
        embedding_size = 200
        without_break_options = True
        with_years = False
        embedding_conf = _emedding_init(embedding_size, without_break_options, with_years)
        embedding_conf.conn = conn
        embedding_conf.cur = cur
        embedding_conf.set_language(_generate_lang_db(conn, cur))
        data = []
        datam = []
        #cols = ['dataset_name', 'top_primitive','top_accuracy', 'rec_algo','ers_accuracy_ratio','reco_family','ers_family_accuracy_ratio','rf_accuracy_ratio','rf_family_acc_ratio']
        cols = ['dataset_name', 'top_primitive','top_accuracy', 'rec_algo','ers_accuracy_ratio','reco_family','ers_family_accuracy_ratio','algo','dist','rank']
        avg_match_score = 0.0
        avg_baseline_score = 0.0
        avg_family_match_score = 0.0
        avg_family_baseline_score = 0.0
        avg_max = 0.0
        avg_min_ratio = 0.0
        num = 0
        recall = 1
        datasets_ids = _get_dataset_ids(conn, cur)  # return tuples of id and name
        # with open(embedding_conf.output_path + '/results_list_nb_gilad_2311_t2.csv', 'a') as f:
        #     writer = csv.writer(f)
        #     writer.writerow(cols)
        dataset_ind = 0
        for dataset_id in datasets_ids:

            # if dataset_ind > 1:
            #     break


            print("dataset:", dataset_id[1])
            data_algos = _get_all_algos_for_dataset(embedding_conf, dataset_id[0])  # returns a list of tuples [algo,dist,index]
            if len(data_algos) < 1:
                print('no_algos_for_dataset')
                continue

            expected_algos = _get_expected_algos_for_dataset(embedding_conf, dataset_id[0])  # returns the norm form or primitives algos suitable for this dataset + accuracy
            # sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
            #match_score = _calc_match_score_family(data_algos, expected_algos, dataset_id[0], embedding_conf, recall)
            dataset_res = _calc_match_score_family_hyb(data_algos, expected_algos, dataset_id[0], embedding_conf, recall)
            data.extend(dataset_res)
            dataset_ind = dataset_ind + 1
            # writer.writerow(line)
            # linem = []
            # linem.extend(expected_algos)
            # for item in data_algos:
            #     linem.extend([item[0], item[1]])  # norm primitive name, distance
            # linem.append(match_score)
            # datam.append(linem)

        df = pd.DataFrame(data, columns=cols)
        df.to_csv(
            embedding_conf.output_path + '/results_family_hyb' + str(recall) + '.csv')

        family_df = _create_hyb_table(df,conn,cur)
        family_df.to_csv(embedding_conf.output_path + '/results_family_hyb2' + str(recall) + '.csv')


def _create_hyb_table(df,conn,cur):
    datasets_ids = df.dataset_name.unique()  # return tuples of id and name
    family_ids = df.reco_family.unique()
    data = []
    for dataset in datasets_ids:
        for family in family_ids:
            min_ind = df['rank'].where((df['dataset_name'] == dataset) & (df['reco_family'] == family)).min()
            max_dist = df['dist'].where((df['dataset_name'] == dataset) & (df['reco_family'] == family)).max()
            actual = df['ers_family_accuracy_ratio'].where((df['dataset_name'] == dataset) & (df['reco_family'] == family)).max()
            max_score = df['top_accuracy'].where((df['dataset_name'] == dataset) & (df['reco_family'] == family)).max()
            data.append([dataset,family,min_ind,max_dist,actual,max_score])
    cols = ['dataset_name','family','min_rec_ind','max_rec_dist','max_rec_acc_rat','max_score']
    df = pd.DataFrame(data, columns=cols)
    return df


def generate_data_Gilad():
    """
    This function evaluates the results for Fernandes dataset
    """

    if platform.platform().lower().find('win') == -1:
        conn, cur = createdb.init_db()
        embedding_size = 200
        without_break_options = True
        with_years = False
        embedding_conf = _emedding_init(embedding_size, without_break_options,with_years)
        embedding_conf.conn = conn
        embedding_conf.cur = cur
        embedding_conf.set_language(_generate_lang_db(conn, cur))
        data = []
        datam = []
        cols = ['dataset_name', 'top_accuracy', 'bottom_accuracy_ratio', 'ers_accuracy_ratio','rf_accuracy_ratio']
        colsm = ['dataset_name', 'top1_primitive_name', 'top1_primitive_normal_name', 'top1_primitive_normal_name2',
                'top1_accuracy', 'top2_primitive_name', 'top2_primitive_normal_name', 'top2_primitive_normal_name2',
                'top2_accuracy']
        for i in range(embedding_conf.max_algo):
            colsm.extend(["rec_" + str(i), "dist_" + str(i)])
        colsm.append('match_score')
        avg_match_score = 0.0
        avg_baseline_score = 0.0
        avg_max = 0.0
        avg_min_ratio = 0.0
        num = 0
        recall = 9
        datasets_ids = _get_dataset_ids(conn, cur)  # return tuples of id and name
        # with open(embedding_conf.output_path + '/results_list_nb_gilad_2311_t2.csv', 'a') as f:
        #     writer = csv.writer(f)
        #     writer.writerow(cols)
        for dataset_id in datasets_ids:
            print("dataset:", dataset_id[1])
            data_algos = _get_algos_for_dataset(embedding_conf, dataset_id[0])  # returns a dictionary of norm form of primitives algo which are closest + distance
            if len(data_algos) < 1:
                print('no_algos_for_dataset')
                continue

            expected_algos = _get_expected_algos_for_dataset(embedding_conf, dataset_id[0])  # returns the norm form or primitives algos suitable for this dataset + accuracy
            # sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
            match_score = _calc_match_score(data_algos, expected_algos, dataset_id[0], embedding_conf,recall)
            base_score = _calc_baseline_score(expected_algos, dataset_id[0], embedding_conf)
            max_score = expected_algos[4]
            min_max_score = _get_min_max_dataset_score(embedding_conf,dataset_id[0])
            min_score = min_max_score[1]
            max_score = min_max_score[0]
            min_score_ratio = kbu.to_number(min_score) / (kbu.to_number(max_score)*1.0)
            avg_max = avg_max + kbu.to_number(max_score)
            avg_min_ratio = avg_min_ratio + min_score_ratio
            avg_match_score = avg_match_score + match_score
            avg_baseline_score = avg_baseline_score + base_score
            num = num + 1
            print(expected_algos[0], str(match_score), str(base_score),str(min_score_ratio))
            # line = [dataset_id[1]]  #dataset name
            line = [expected_algos[0],min_max_score[0],min_score_ratio,match_score,base_score]
            data.append(line)
            #writer.writerow(line)
            linem = []
            linem.extend(expected_algos)
            for item in data_algos:
                linem.extend([item[0], item[1]])  # norm primitive name, distance
            linem.append(match_score)
            datam.append(linem)

        if num > 0:
            print("score ", (avg_match_score / (1.0 * num)), " base score ", (avg_baseline_score / (1.0 * num)), " num ", num)
            avg_max = avg_max / (1.0*num)
            avg_match_score = avg_match_score / (1.0*num)
            avg_baseline_score = avg_baseline_score / (1.0*num)
            avg_min_ratio = avg_min_ratio / (1.0*num)
            line = ['average',avg_max, avg_min_ratio, avg_match_score, avg_baseline_score]
            data.append(line)
            #writer.writerow(line)

        df = pd.DataFrame(data, columns=cols)
        df.to_csv(embedding_conf.output_path + '/results_list_nb_gilad_2611_stem_dictf_recall'+str(recall)+'.csv')
        dfm = pd.DataFrame(datam, columns=colsm)
        dfm.to_csv(embedding_conf.output_path + '/results_list_nb_prim_2611_stem_dictf_recall'+str(recall)+'.csv')

def _get_dataset_ids_by_name(conn, cur,names):
    conn, cur = createdb.init_db()
    sql = """SELECT dataset_id,dataset_name FROM datasets where dataset_name = %s;"""
    datasets = []
    for name in names:
        try:
            cur.execute(sql,(name,))
            dataset = cur.fetchone()
            datasets.append(dataset)
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "select datasets by name")
            return

    return datasets


def calculate_single_datasets_score():
    conn, cur = createdb.init_db()
    embedding_size = 200
    without_break_options = True
    with_years = False
    embedding_conf = _emedding_init(embedding_size, without_break_options,with_years)
    embedding_conf.conn = conn
    embedding_conf.cur = cur
    embedding_conf.set_language(_generate_lang_db(conn, cur))
    data = []
    cols = ['dataset_name', 'top1_primitive_name', 'top1_primitive_normal_name', 'top1_primitive_normal_name2',
            'top1_accuracy', 'top2_primitive_name', 'top2_primitive_normal_name', 'top2_primitive_normal_name2',
            'top2_accuracy']
    colsg = ['dataset_name', 'ers_accuracy_ratio', 'rf_accuracy_ratio']
    datag=[]
    for i in range(embedding_conf.max_algo):
        cols.extend(["rec_" + str(i), "dist_" + str(i)])
    cols.append('match_score')
    avg_match_score = 0.0
    avg_baseline_score = 0.0
    num = 0
    names = ['pittsburg-bridges-TYPE']
    #'balloons','teaching','wine-quality-white','pittsburg-bridges-TYPE']
    datasets_ids = _get_dataset_ids_by_name(conn, cur,names)  # return tuples of id and name
    with open(embedding_conf.output_path + '/results_nb_otmp.csv', 'a') as f:
        writer = csv.writer(f)
        writer.writerow(cols)
        for dataset_id in datasets_ids:
            print("dataset:", dataset_id[1])
            data_algos = _get_algos_for_dataset(embedding_conf, dataset_id[
                0])  # returns a dictionary of norm form of primitives algo which are closest + distance
            if len(data_algos) < 1:
                print('no_algos_for_dataset')
                continue

            expected_algos = _get_expected_algos_for_dataset(embedding_conf, dataset_id[
                0])  # returns the norm form or primitives algos suitable for this dataset + accuracy
            # sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
            match_score = _calc_match_score(data_algos, expected_algos, dataset_id[0], embedding_conf)
            base_score = _calc_baseline_score(expected_algos, dataset_id[0], embedding_conf)
            avg_match_score = avg_match_score + match_score
            avg_baseline_score = avg_baseline_score + base_score
            num = num + 1
            print(expected_algos[0], str(match_score), str(base_score))
            # line = [dataset_id[1]]  #dataset name
            line = []
            line.extend(expected_algos)
            for item in data_algos:
                line.extend([item[0], item[1]])  # norm primitive name, distance
            line.append(match_score)
            data.append(line)
            datag.append([dataset_id[1],str(match_score),str(base_score)])
            writer.writerow(line)

    if num > 0:
        print(
        "score ", (avg_match_score / (1.0 * num)), " base score ", (avg_baseline_score / (1.0 * num)), " num ", num)

    df = pd.DataFrame(data, columns=cols)
    df.to_csv(embedding_conf.output_path + '/results_nbatmp.csv')

    df = pd.DataFrame(datag,columns=colsg)
    df.to_csv(embedding_conf.output_path + '/results_nbgtmp.csv')

def evaluate_fernandes_dataset():
    """
    This function evaluates the results for Fernandes dataset
    """

    if platform.platform().lower().find('win') == -1:
        conn, cur = createdb.init_db()
        embedding_size = 200
        without_break_options = True
        embedding_conf = _emedding_init(embedding_size, without_break_options)
        embedding_conf.conn = conn
        embedding_conf.cur = cur
        embedding_conf.set_language(_generate_lang_db(conn,cur))
        data = []
        cols = ['dataset_name','top1_primitive_name','top1_primitive_normal_name','top1_primitive_normal_name2','top1_accuracy','top2_primitive_name','top2_primitive_normal_name','top2_primitive_normal_name2','top2_accuracy']
        for i in range(embedding_conf.max_algo):
            cols.extend(["rec_"+str(i),"dist_"+str(i)])
        cols.append('match_score')
        avg_match_score = 0.0
        avg_baseline_score = 0.0
        num = 0
        datasets_ids = _get_dataset_ids(conn, cur)  #return tuples of id and name
        with open(embedding_conf.output_path+'/results_nb_o.csv','a') as f:
            writer = csv.writer(f)
            writer.writerow(cols)
            for dataset_id in datasets_ids:
                print("dataset:",dataset_id[1])
                data_algos = _get_algos_for_dataset(embedding_conf,dataset_id[0])   #returns a dictionary of norm form of primitives algo which are closest + distance
                if len(data_algos) < 1:
                    print('no_algos_for_dataset')
                    continue

                expected_algos = _get_expected_algos_for_dataset(embedding_conf,dataset_id[0])  #returns the norm form or primitives algos suitable for this dataset + accuracy
                #sql = """SELECT dataset_name,top1_primitive_name,top1_primitive_normal_name,top1_primitive_normal_name2,top1_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy,top2_primitive_name,top2_primitive_normal_name,top2_primitive_normal_name2,top2_accuracy FROM datasets WHERE dataset_id = %s;"""
                match_score = _calc_match_score(data_algos,expected_algos,dataset_id[0],embedding_conf)
                base_score = _calc_baseline_score(expected_algos,dataset_id[0],embedding_conf)
                avg_match_score = avg_match_score + match_score
                avg_baseline_score = avg_baseline_score + base_score
                num = num + 1
                print(expected_algos[0],str(match_score),str(base_score))
                #line = [dataset_id[1]]  #dataset name
                line = []
                line.extend(expected_algos)
                for item in data_algos:
                    line.extend([item[0],item[1]])   #norm primitive name, distance
                line.append(match_score)
                data.append(line)
                writer.writerow(line)

        if num > 0:
            print("score ", (avg_match_score / (1.0 * num))," base score ",(avg_baseline_score / (1.0 * num))," num ",num)

        df = pd.DataFrame(data, columns=cols)
        df.to_csv(embedding_conf.output_path+'/results_nb.csv')

def _generate_lang_db3(conn,cur):
    lang = []
    lang_dict = {}
    sql = """SELECT primitive_normal_name FROM primitives;"""
    sql2 = """SELECT primitive_normal_name2 FROM primitives;"""
    sql3 = """SELECT primitive_normal_name3 FROM primitives WHERE primitive_normal_name3 IS NOT NULL;"""
    try:
        cur.execute(sql)
        primitives = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "generate lang")
        return

    for primitive in primitives:
        if (primitive[0]  not in lang_dict) and (len(primitive[0])>1):
            lang_dict[primitive[0]]=1
            lang.append(primitive[0])

    try:
        cur.execute(sql2)
        primitives = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "generate lang")
        return

    for primitive in primitives:
        if primitive[0] not in lang_dict and (len(primitive[0])>1):
            lang_dict[primitive[0]] = 1
            lang.append(primitive[0])

    try:
        cur.execute(sql3)
        primitives = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "generate lang")
        return

    for primitive in primitives:
        if (primitive[0] not in lang_dict) and (len(primitive[0]) > 1):
            lang_dict[primitive[0]] = 1
            lang.append(primitive[0])

    return lang

def _generate_lang_db(conn,cur):
    lang = []
    lang_dict = {}
    sql = """SELECT primitive_normal_name FROM primitives;"""
    sql2 = """SELECT primitive_normal_name2 FROM primitives;"""
    try:
        cur.execute(sql)
        primitives = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "generate lang")
        return

    for primitive in primitives:
        if (primitive[0] is not lang_dict) and (len(primitive[0])>1):
        #if (primitive[0] not in lang_dict) and (len(primitive[0]) > 1):
            lang_dict[primitive[0]]=1
            lang.append(primitive[0])

    try:
        cur.execute(sql2)
        primitives = cur.fetchall()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "generate lang")
        return

    for primitive in primitives:
        if primitive[0] is not lang_dict and (len(primitive[0])>1):
        #if primitive[0] not in lang_dict and (len(primitive[0]) > 1):
            lang_dict[primitive[0]] = 1
            lang.append(primitive[0])

    return lang

def _read_words_from_vocab(vocab_file):
  with open(vocab_file, 'r') as read_file:
    words = []
    for line in read_file:
      words.append(line.rstrip().split(' ')[0])
  return words


def _read_vectors(vectors_file):
  with open(vectors_file, 'r') as read_file:
    vectors = {}
    for line in read_file:
      vals = line.rstrip().split(' ')
      vectors[vals[0]] = map(float, vals[1:])
  return vectors, vals

def _emedding_init(embedding_size=50, without_break=False,with_years=True):
  """
  Initialize all embedding data structures
  """

  conf = EvalConfig(embedding_size, without_break,with_years)
  words = _read_words_from_vocab(conf.vocab_file)
  vectors, vals = _read_vectors(conf.vectors_file)
  vocab = {w: idx for idx, w in enumerate(words)}
  ivocab = {idx: w for idx, w in enumerate(words)}
  weights = np.zeros((len(words), (len(vals) - 1)))
  for word, vec in vectors.items():
    if word == '<unk>':
      continue
    vec_list = list(vec)
    vec_size = len(vec_list)
    if vec_size > 0:
      weights[vocab[word], :] = vec_list[:]

  # normalize each word vector to unit variance
  w_norm = (weights.T / (np.sum(weights ** 2, 1) ** 0.5)).T
  conf.set_embedding(w_norm, vocab, ivocab)
  conf._set_additional_stopwords()
  return conf



def _remove_additional(word_list,conf):
    new_word_list = []
    for word in word_list:
        if word.lower() not in conf.additional_dict:
            new_word_list.append(word.lower())

    return new_word_list

def _remove_stopwords(word_list):
  """
  This function removes the stop words from the dataset description
  """
  punctuation = list(string.punctuation)
  stop = nltk.corpus.stopwords.words("english") + punctuation
  processed_word_list = []
  for word in word_list:
    word = word.lower()  # in case they are not all lower cased
    if word not in stop:
      processed_word_list.append(word)
  return processed_word_list

def _get_bigrams(myString):
    tokenizer = nltk.WordPunctTokenizer()
    tokens = tokenizer.tokenize(myString)
    stemmer = nltk.PorterStemmer()
    bigram_finder = nltk.BigramCollocationFinder.from_words(tokens)
    bigrams = bigram_finder.nbest(nltk.BigramAssocMeasures.chi_sq, 500)

    for bigram_tuple in bigrams:
        x = "%s %s" % bigram_tuple
        tokens.append(x)

    result = [' '.join([stemmer.stem(w).lower() for w in x.split()]) for x in tokens if x.lower() not in stopwords.words('english') and len(x) > 8]
    return result

def _get_dataset_description_words_fernandes(conf,dataset_id):
    evaluation_words = []
    sql = """SELECT dataset_normal,title FROM datasets WHERE dataset_id = %s;"""
    try:
        conf.cur.execute(sql,(dataset_id,))
        name,title = conf.cur.fetchone()
    except (Exception, psycopg2.DatabaseError) as error:
        print(error, "get dataset description",dataset_id)
        return

    #if conf.require_title and len(title)<1:
    if title is None:
        return[]

    title_words = title.split()
    title_words = _remove_stopwords(title_words)
    title_words = _remove_additional(title_words,conf)
    evaluation_words.extend(title_words)

    title_words_bi_grams = _get_bigrams(title.lower())
    for word in title_words_bi_grams:
        con_word = word[0]+"_"+word[1]
        evaluation_words.extend(con_word)

    #get vocabularly words which are close to title words
    matched_vocab_words_name = get_vocab_words_for_dataset_norm_name(conf,name)
    matched_vocab_words_name = _remove_additional(matched_vocab_words_name,conf)
    evaluation_words.extend(matched_vocab_words_name)
    evaluation_words = kbu.remove_duplicates_from_list(evaluation_words)
    return evaluation_words

def _get_dataset_description_words(conf,dataset_id):
    if conf.dataset_source == 'fernandes':
        return _get_dataset_description_words_fernandes(conf,dataset_id)


def _get_all_algos_for_dataset(conf, dataset_id, full=False):
    """
    This function returns a list (dictionary - normalized name,distance?) of algorithms which may be effective for this dataset
    """
    evaluation_words = _get_dataset_description_words(conf, dataset_id)

    if evaluation_words is None:
        print("no_evaluation_words")
        return {}

    algos_dist_obj = AlgoDist(conf)
    matched_words = 0
    for word in evaluation_words:
        word = kbu.core_normalization(word)
        if (word in conf.vocab) and (len(word) > 1):  # changed
            algos_dist_obj.update_algo_distance_fernandes(word)
            matched_words = matched_words + 1
            print('matched word ', word)

    if algos_dist_obj is None:
        size = 0
    else:
        size = len(algos_dist_obj.nearest_algo_dict)

    if matched_words < 1:
        return {}

    print("matched words ", matched_words, " candidate models ", size)
    nearest_algo_res = sorted(algos_dist_obj.nearest_algo_dict.items(), key=itemgetter(1),
                               reverse=True)  # not sure about reverse...
    # # for algo in nearest_algo_res:
    # #   algo[1] = algo[1] / (matched_words * 1.0)
    # if full:

    res_algo = []
    i = 0
    for algo in nearest_algo_res:
        res_algo.append([algo[0],algo[1],i])
        i = i + 1


    return res_algo

    #return algos_dist_obj.nearest_algo_dict.items()

def _get_algos_for_dataset(conf,dataset_id,full=False):
      """
      This function returns a list (dictionary - normalized name,distance?) of algorithms which may be effective for this dataset
      """
      evaluation_words = _get_dataset_description_words(conf,dataset_id)

      if evaluation_words is None:
          print("no_evaluation_words")
          return {}


      algos_dist_obj = AlgoDist(conf)
      matched_words = 0
      for word in evaluation_words:
        word = kbu.core_normalization(word)
        if (word in conf.vocab) and (len(word)>1):   #changed
           algos_dist_obj.update_algo_distance_fernandes(word)
           matched_words = matched_words + 1
           print('matched word ',word)

      if algos_dist_obj is None:
          size = 0
      else:
          size = len(algos_dist_obj.nearest_algo_dict)

      if matched_words < 1:
          return {}

      print("matched words ",matched_words," candidate models ",size)
      nearest_algo_res = sorted(algos_dist_obj.nearest_algo_dict.items(), key=itemgetter(1), reverse=True)  #not sure about reverse...
      # for algo in nearest_algo_res:
      #   algo[1] = algo[1] / (matched_words * 1.0)
      if full:
          return nearest_algo_res

      return nearest_algo_res[:conf.max_algo]

def get_vocab_words(vocab_file):
    vocab_words = {}
    with open(vocab_file, "r") as ins:
        for line in ins:
            words = line.split()
            vocab_words[words[0]]=words[1]
    return vocab_words

def get_vocab_words_for_dataset_norm_name(conf,dataset_normal_name):
    st = LancasterStemmer()
    words = []
    for key, freq in conf.vocab.iteritems():
        if (len(key)>1):
            key_stem = st.stem(key.decode('utf-8'))
            name_stem = st.stem(dataset_normal_name.decode('utf-8'))
            #distance = kbu.levenshtein_distance(key,dataset_normal_name)
            distance = kbu.levenshtein_distance(key_stem, name_stem)
            #max_len = max(len(key), len(dataset_normal_name))
            max_len = max(len(key_stem), len(name_stem))
            if (distance / (1.0 * max_len) < 0.25) and (int(freq) >= 10):        #was 0.35, changed to 0.2
                words.append(key)
                #print('key ',key)
    return words

def find_dataset_keyword_match(datasets_normals,vocab):
    """
    This function get a glove vocabulary dictionary and a list of datasets normal names
    and returns the number of database with matching names and the matching voabulary names + frequency
    """
    number_of_matches = 0
    match_dict = defaultdict(list)
    for dataset in datasets_normals:
        if platform.platform().lower().find('win') == -1:
            dataset = dataset[0]
        found_match = False
        for key, freq in vocab.iteritems():
            distance = kbu.levenshtein_distance(key,dataset)
            max_len = max(len(key), len(dataset))
            if (distance / (1.0 * max_len) < 0.35) and (int(freq) >= 10):
                ndist = str('%.2f' % (distance / (1.0 * max_len)))
                match_dict[dataset].append(str(key)+"_"+str(freq)+"_"+ndist)
                found_match = True
                print(dataset,str(key)+"_"+str(freq)+"_"+ndist)
        if found_match:
            number_of_matches = number_of_matches + 1

    return number_of_matches, match_dict

def load_list_from_file(fname):
    with open(fname,'r') as f:
        content = f.readlines()
    content = [x.strip() for x in content]

    return content

def find_dataset_title_to_glove_voc_match():
    embedding_size = 200
    without_break_options = [True, False]
    conf = EvalConfig(embedding_size, True, [])
    if platform.platform().lower().find('win') == -1:
        conn, cur = createdb.init_db()
        datasets_normals = get_dataset_normals(conn,cur)
        save_list_to_file(conf.output_path + "/dataset_name.txt", datasets_normals)
    else:
        conf.output_path = conf.out_pathw
        conf.path = conf.glove_pathw
        datasets_normals = load_list_from_file(conf.output_path + "/dataset_name.txt")


    for break_option in without_break_options:
        conf = EvalConfig(embedding_size, break_option,[])
        vocab = get_vocab_words(conf.vocab_file)
        match_dictionary = {}
        save_dictionary_to_file(conf.output_path + "/dataset_glove_match_" + str(break_option) + "_" + str(embedding_size) + ".txt",match_dictionary)
        number_of_matches, match_dictionary = find_dataset_keyword_match(datasets_normals,vocab)
        print("break option ",str(break_option)," matches ",number_of_matches)
        save_dictionary_to_file(conf.output_path+"/dataset_glove_match_"+str(break_option)+"_"+str(embedding_size)+".txt",match_dictionary)


def evaluate_problems():
    embedding_size = 200
    without_break = True
    evaluation_words = ['breast_cancer']
    conf = EvalConfig(embedding_size, without_break,evaluation_words)
    vectors_file = conf.vectors_file
    vocab_file = conf.vocab_file
    # lang = createdb.darpa_lang()
    # apps = createdb.generate_lang(lang.lang_path+lang.apps_file)
    # algos = createdb.generate_lang(lang.lang_path + lang.algo_file)
    conn, cur = createdb.init_db()
    algos = createdb.get_primitives_algos(conn, cur)
    apps = []

    print_expected_results(conf, conn, cur)

    with open(vocab_file, 'r') as f:
        words = [x.rstrip().split(' ')[0] for x in f.readlines()]
    with open(vectors_file, 'r') as f:
        vectors = {}
        for line in f:
            vals = line.rstrip().split(' ')
            vectors[vals[0]] = map(float, vals[1:])

    vocab_size = len(words)
    vocab = {w: idx for idx, w in enumerate(words)}
    ivocab = {idx: w for idx, w in enumerate(words)}

    vector_dim = len(vectors[ivocab[0]])
    W = np.zeros((vocab_size, vector_dim))
    for word, v in vectors.iteritems():
        if word == '<unk>':
            continue
        W[vocab[word], :] = v

    # normalize each word vector to unit variance
    W_norm = np.zeros(W.shape)
    d = (np.sum(W ** 2, 1) ** (0.5))
    W_norm = (W.T / d).T
    evaluate_vectors(W_norm, vocab, ivocab, apps, algos, conf)

    return

def main():
    #evaluate_problems()
    #find_dataset_title_to_glove_voc_match()
    #evaluate_fernandes_dataset()
    #generate_data_Gilad()
    #calculate_single_datasets_score()
    #get_list_of_fernandes_model_keywords()
    #add_avg_accuracy_to_res()
    #family = True
    #MRR_evaluation(family)
    #MRR_family_randomforest()
    #generate_data_family()

    generate_data_family_hybrid()




def print_expected_results(conf,conn,cur):
    sql = """SELECT dataset_name,top1_primitive_normal_name,top1_primitive_normal_name2,top2_primitive_normal_name,top2_primitive_normal_name2,top3_primitive_normal_name,top3_primitive_normal_name2 FROM datasets WHERE dataset_normal like %s;"""
    for word in conf.evaluation_words:
        # print("word ",word)
        word_str = 'breast_cancer_wisc'
        try:
            cur.execute(sql,(word_str,))
            datasets = cur.fetchall()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error, "print expected")
            return

        for dataset in datasets:
            for i in range(7):
                print(dataset[i])
        print("\n")


def evaluate_vectors(W, vocab, ivocab,apps,algos,conf):
    """Evaluate the trained word vectors on a variety of tasks"""
    N = conf.max_eval
    with open(conf.output_file, "w") as out_file:
        outstr = "evaluated_word,embedding_size,keyword_break_yes_no,[keyword,keyword_distance]"+'\n'
        out_file.write(outstr)
        for word in conf.evaluation_words:
            algo_num = 0
            problem_num = 0
            ind = vocab[word]
            pred_vec = W[ind,:]
            dist = np.dot(W, pred_vec.T)
            dist[ind] = -np.Inf
            a = np.argsort(-dist)[:N]
            nearest_algo = []
            nearest_problem = []
            for x in a:
                if ivocab[x] in algos:
                    algo_num = algo_num + 1
                    if algo_num < conf.max_algo:
                        dist[x] = format(dist[x],'.2f')
                        nearest_algo.append({'val':ivocab[x],'dist':dist[x]})
                if ivocab[x] in apps:
                    problem_num = problem_num + 1
                    if (problem_num < conf.max_problem):
                        dist[x] = format(dist[x], '.2f')
                        nearest_problem.append({'val': ivocab[x], 'dist': dist[x]})

            outstr = ''
            if len(conf.nbs)>0:
                nbs = ','+conf.nbs
            else:
                nbs = ','+'b'
            outstr = outstr + word+','+str(conf.embedding_size)+nbs
            for item in nearest_algo:
                outstr = outstr+','+item['val']+','+str(item['dist'])
            print(outstr)
            outstr = outstr +'\n'
            out_file.write(outstr)
            outstr = ''
            outstr = outstr+word + ',' +str(conf.embedding_size)+ nbs
            for item in nearest_problem:
                outstr = outstr + ',' + item['val'] + ',' + str(item['dist'])
            print(outstr)
            outstr = outstr + '\n'
            out_file.write(outstr)



    return



if __name__ == "__main__":
    main()

