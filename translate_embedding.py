
def main():
    path = r'C:\Asi\BD\Lior\Code\darpa\darpa\glove'
    vectors_file = "vectors"
    vocab_file = "vocab"
    replace_delimiter(path+"\\"+vectors_file+".txt",path+"\\"+vectors_file+"_out.txt",True)
    replace_delimiter(path + "\\" + vocab_file + ".txt", path + "\\" + vocab_file + "_out.txt")
    return

def replace_delimiter(in_file,out_file,remove_first_word=False):
    #remove_first_word is False in vectors file
    t = 0
    with open(in_file) as in_vectors_file:
        with open(out_file,"w") as out_vectors_file:
            lines = in_vectors_file.readlines()
            for line in lines:
                if remove_first_word:
                    line = line.split(" ",1)[1]
                # else:
                #     words = line.split(" ")
                #     words[0] = words[0].strip()
                #     words[1] = words[1].strip()
                #     line = " ".join(words)
                #     line = line +"\n"
                #     print(line)
                #     print(words[0],words[1])
                line = line.replace(" ", "\t")
                if (remove_first_word==False):
                    print(line)
                out_vectors_file.write(line)
    return


if __name__ == '__main__':
    main()