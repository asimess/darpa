import psycopg2
import datetime

def generate_year_dict():
    dict = {}
    for year in range(1961,2000):
        dict[year] = 1
    dict[0] = 1
    for year in range(2000,2019):
        dict[year] = year - 2000 + 2

    return dict


def generate_corpus(conn,cur,output_file,include_break = True,year_factor = True):

    if year_factor:
        year_dict = generate_year_dict()

    with open(output_file,"w") as out_file:
        sql = """SELECT paper_id,year FROM papers; """
        try:
            cur.execute(sql)
            papers = cur.fetchall()
        except:
            print("can't select papers from db")

        if include_break:
            sql_keyword_id = """SELECT keyword_id FROM papers_keywords WHERE paper_id = %s; """
        else:
            sql_keyword_id = """SELECT keyword_id FROM papers_keywords WHERE (paper_id = %s AND keyword_type != %s);  """
        sql_keyword_str = """SELECT normalized_keyword_string FROM keywords WHERE keyword_id = %s; """

        for paper in papers:
            paper_str = []
            try:
                if include_break:
                    cur.execute(sql_keyword_id,(paper[0],))
                else:
                    cur.execute(sql_keyword_id, (paper[0],'T',))
                keywords = cur.fetchall()
            except:
                print("can't fetch keywords for paper: ",paper[0])
            if keywords is not None:
                for keyword in keywords:
                    try:
                        cur.execute(sql_keyword_str,(keyword[0],))
                        keyword_str = cur.fetchone()
                        paper_str.append(keyword_str[0])
                    except:
                        print("can't fetch keyword ",keyword[0])
                if (len(paper_str)>0):
                    for i in range(year_dict[paper[1]]):
                        out_str = " ".join(paper_str)
                        out_file.write(out_str+"\n")
            else:
                print("no keyword for paper ",paper[0])


def main():
    pathu = "/home/ise/work/EngineeringVillage/"
    pathw = "C:\Asi\BD\Lior\Code\darpa\darpa"
    path = pathu
    i = datetime.datetime.now()
    #output_file = "papers_glove"+i.day+"txt"
    output_file = "papers_glove_nobreak_years.txt"
    include_break = False
    years_factor = True

    try:
        # conn = psycopg2.connect(dbname='darpa', user='yoochose', host='localhost', password='1234')
        conn = psycopg2.connect(dbname='ise', user='ise', host='localhost', password='ise')
        cur = conn.cursor()
    except:
        print "I am unable to connect to the database"
        return

    generate_corpus(conn,cur,path+output_file,include_break,years_factor)
    return
    stop = True


if __name__ == '__main__':
    main()