#!/usr/bin/env python
# -*- coding: utf-8 -*-
import logging

logger = logging.getLogger(__name__)


def core_normalization(keyword_in):
  """
  Normalize keywords.

  Parameters
  ----------
  keyword_in : str
      The input keyword for normalization

  Returns
  -------
  Normalized string : str
  """

  keyword_in = keyword_in.lower()
  keyword_in = keyword_in.replace('\r', '')
  keyword_in = keyword_in.replace('\n', '')
  #keyword_in = keyword_in.replace('–', '_')
  keyword_in = keyword_in.replace('-', '_')
  keyword_in = keyword_in.replace(' ', '_')
  return keyword_in

def remove_duplicates_from_list(input_list):
  """
  This function remove duplicate items from a list
  """
  new_list = []
  input_list.sort()

  for item in input_list:
    if item not in new_list:
      new_list.append(item)

  return new_list

def nformat(value):
    return "%.2f" % value

def to_number(s):
    """
    This function coversts a str to float if possible otherwise returns -1
    """
    try:
        fl_str = float(s)  # for int, long and float
    except ValueError:
        return str('-1.0')

    return fl_str

def levenshtein_distance(str1, str2):
  """
  Levenshtein distance between two strings

  Parameters
  ----------
  str1 : str
      The first string
  str2 : str
      The second string

  Returns
  -------
  int
      The distance
  """

  if len(str1) > len(str2):
    str1, str2 = str2, str1

  distances = list(range(len(str1) + 1))
  for i, char2 in enumerate(str2):
    distances_ = [i + 1]
    for j, char1 in enumerate(str1):
      if char1 == char2:
        distances_.append(distances[j])
      else:
        distances_.append(1 + min((distances[j], distances[j + 1], distances_[-1])))
    distances = distances_
  return distances[-1]


def find_stat_match(lang_arr, word):
  """
  Find match strings by adjusted distance

  Parameters
  ----------
  lang_arr : list
      The list of words
  word : str
      Match pattern

  Returns
  -------
  list
      Matched words
  """

  match = []
  for lang_arr_item in lang_arr:
    distance = levenshtein_distance(lang_arr_item, word)
    max_len = max(len(lang_arr_item), len(word))
    if distance / (1.0 * max_len) < 0.35:
      match.append(lang_arr_item)
  return match


def find_match(lang_arr, word):
  """
  Find match strings by distance

  Parameters
  ----------
  lang_arr : list
      The list of words
  word : str
      Match pattern

  Returns
  -------
  list
      Matched words
  """

  match = []
  max_distance = 4
  for lang_arr_item in lang_arr:
    distance = levenshtein_distance(lang_arr_item, word)
    if distance < max_distance and len(lang_arr_item) >= 3 * distance:
      match.append(lang_arr_item)
  return match


def task_logging(current_logger, dataset, task_type, task_subtype, task_description):
  """
  Logging task

  Parameters
  ----------
  current_logger : logger
      The logger of context
  dataset : dataset
      The dataset
  task_type : str
      Type of task
  task_subtype : str
      Subtype of task
  task_description : str
      Description of task

  """

  current_logger.info("task_type: %s", task_type)
  current_logger.info("task_subtype: %s", task_subtype)
  current_logger.info("task description: %s" % task_description)

  with dataset:
    current_logger.info("dataset.description: %s", dataset.description)

    current_logger.info("train_samples: %s", dataset.get_train_samples())
    current_logger.info("train_targets: %s", dataset.get_train_targets())

    #primitive = list(primitives.store.list())[0]

    #current_logger.info("primitive: %s", primitive.annotation)


def generate_lang(input_file):
  """
  Generate language from file

  Parameters
  ----------
  input_file : str
      Input file path

  Returns
  -------
  list
      List of languages
  """

  with open(input_file, "r", encoding="utf8", errors="ignore") as file:
    # split line and normalize
    return [core_normalization(line) for line in file.readlines()]
