#!/usr/bin/env python
# -*- coding: utf-8 -*-

import requests

def get_primitive_by_keyword(key,search_fields):
    primitives = []
    r = requests.get('https://marvin.datadrivendiscovery.org/search_primitives?keyword=%27%svm%%27&search_fields=common_name', auth=('***', '***'))  #replace credentials
    print(r.text)
    # process responses get list of primitives id
    return primitives

def get_primitives_by_keyword(keywords,search_fields):
    primitives = []
    for key in keywords:
        get_primitive_by_keyword(key,search_fields)
    return primitives  #return a list of primitives id

def get_algos_rs(dataset_id,problem_id):
    algos = []
    return algos

def get_algos_mm(dataset_id):
    algos = []
    return algos

def combine_rs_and_mm(algo_rs,algo_mm):
    algos = []
    return algos

def get_primitives_for_algos(algos):
    primitives = []
    return primitives

def get_primitives(dataset_id,problem_id):
    algo_rs = get_algos_rs(dataset_id,problem_id)
    algo_mm = get_algos_mm(dataset_id,problem_id)
    algos = combine_rs_and_mm(algo_rs,algo_mm)
    primitives = get_primitives_for_algos(algos)
    return primitives

def test():
    keyw = '%27%svm%%27'
    #r = requests.get('https://marvin.datadrivendiscovery.org/search_primitives?keyword=%27%svm%%27&search_fields=common_name', auth=('amessica', 'Ilovetav'))
    r = requests.get('https://marvin.datadrivendiscovery.org/search_primitives?keyword='+keyw+'&search_fields=common_name',auth=('amessica', 'Ilovetav'))
    print(r.text)


def main():
    run_test = True
    if run_test:
        test()
    else:
        dataset_id = 'r_32_dataset'
        problem_id = None
        primitives = get_primitives(dataset_id,problem_id)
        for primitive_id in primitives:
            print(primitive_id)

if __name__ == "__main__":
    main()

